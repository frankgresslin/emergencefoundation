<?php
/**
 * Single post content template.
 *
 * @package The7
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post;

$config = presscore_config();

$post_classes = array();
if ( $config->get_bool( 'post.fancy_date.enabled' ) ) {
	$post_classes[] = presscore_blog_fancy_date_class();
}
?>

<?php  
	
	$user = wp_get_current_user();
	$allowed_roles = array('administrator', 'stakeholder');
	
	if ( array_intersect( $allowed_roles, $user->roles ) ) {
	
?>

<article id="post-<?php the_ID() ?>" <?php post_class( $post_classes ) ?>>

	<?php
	do_action( 'presscore_before_post_content' );
	
	

	// POST THUMBNAIL / MEDIA
	$hide_thumbnail = (bool) get_post_meta( $post->ID, '_dt_post_options_hide_thumbnail', true );

	if ( has_post_thumbnail() && ! $hide_thumbnail ) {
		$thumbnail_id = get_post_thumbnail_id();
		$video_url = esc_url( get_post_meta( $thumbnail_id, 'dt-video-url', true ) );

		if ( ! $video_url ) {
			$thumb_args = array(
				'class'    => 'alignnone',
				'img_id'   => $thumbnail_id,
				'wrap'     => '<img %IMG_CLASS% %SRC% %SIZE% %IMG_TITLE% %ALT% />',
				'echo'     => false,
			);

			// Thumbnail proportions.
			if ( 'resize' === of_get_option( 'blog-thumbnail_size' ) ) {
				$prop = of_get_option( 'blog-thumbnail_proportions' );
				$width = max( absint( $prop['width'] ), 1 );
				$height = max( absint( $prop['height'] ), 1 );

				$thumb_args['prop'] = $width / $height;
			}

			$post_media_html = presscore_get_blog_post_fancy_date();
			if ( $config->get_bool( 'post.fancy_category.enabled' ) ) {
				$post_media_html .= presscore_get_post_fancy_category();
			}

			$post_media_html .= dt_get_thumb_img( $thumb_args );
			
		} else {
			
			$post_media_html = '<div class="post-video alignnone">' . dt_get_embed( $video_url ) . '</div>';
			
		}
		
		// the7 thumbnail
		// echo '<div class="post-thumbnail">' . $post_media_html . '</div>';
		
		// include thumbnail
		// inc_post_thumbnail();
		
		} elseif ( inc_get_option( 'site_logo' ) ) {
			
			// use logo set in theme customizer if there is no thumbnail
			// inc_post_thumbnail();
		}
		
		

	// POST CONTENT
		echo '<div class="entry-content">';
		the_content();
		wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'the7mk2' ), 'after' => '</div>' ) );
		echo '</div>';




	// POST META
	
	$post_meta = presscore_get_single_posted_on();
	if ( $config->get( 'post.meta.fields.tags' ) ) {
		$post_meta .= presscore_get_post_tags_html();
	}
	
	if ( $post_meta ) {
		
		// the 7 meta
		// echo '<div class="post-meta wf-mobile-collapsed">' . $post_meta . '</div>';
		
		// include meta
		// inc_post_meta();
	}



	// SOCIAL SHARING
	
	// the7 social sharing
	// presscore_display_share_buttons_for_post( 'post' );
	
	// include social shareing
	// inc_social_sharing();
	
	

	// AUTHOR BOX
	
	if ( $config->get( 'post.author_block' ) ) {
		
		// the7 author box		
		// presscore_display_post_author();
		
		// include author box
		// inc_author_box();
	}
	


	// POST NAVIGATION
	
	// the7 post naviagtion
	// echo presscore_new_post_navigation();
	
	// include post navigation
	fg_post_navigation();



	// RELATED POSTS
	
	// the7 related posts
	// presscore_display_related_posts();
	
	// include related posts
	// inc_related_posts();

	do_action( 'presscore_after_post_content' );
	?>

</article>


<?php  
	
		
    } else {
        
        echo do_shortcode( '[vc_row][vc_column width="1/3"][/vc_column][vc_column width="1/3"][vc_column_text][theme-my-login][/vc_column_text][/vc_column][vc_column width="1/3"][/vc_column][/vc_row]' );
        
    }
	
?>