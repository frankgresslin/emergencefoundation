<?php
/**
 * Archive pages.
 *
 * @package The7
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

$config = presscore_config();
$config->set( 'template', 'archive' );
$config->set( 'layout', 'masonry' );
$config->set( 'template.layout.type', 'masonry' );

get_header();
?>
			<!-- Content -->
			<div id="content" class="content" role="main">

				<?php
				
				// load tml login form if user is not a stakeholder
				$user = wp_get_current_user();
				$allowed_roles = array('administrator', 'stakeholder');
				
				if ( array_intersect( $allowed_roles, $user->roles ) || ( 'post' == get_post_type() ) ) {
					
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
	
					if ( ! have_posts() ) {
						get_template_part( 'no-results', 'search' );
	                } else {
						do_action( 'presscore_before_loop' );
	
						// backup config
						$config_backup = $config->get();
	
						// masonry container open
						echo '<div ' . presscore_masonry_container_class( array( 'wf-container' ) ) . presscore_masonry_container_data_atts() . '>';
	                    while ( have_posts() ) {
	                        the_post();
	                        presscore_archive_post_content();
	                        $config->reset( $config_backup );
	                    }
						// masonry container close
						echo '</div>';
	
						dt_paginator();
	
						do_action( 'presscore_after_loop' );
	                }
                
                } else {
	                
	                echo do_shortcode( '[vc_row][vc_column width="1/3"][/vc_column][vc_column width="1/3"][vc_column_text][theme-my-login][/vc_column_text][/vc_column][vc_column width="1/3"][/vc_column][/vc_row]' );
	                
                }
                
                
                
				?>

			</div><!-- #content -->

			<?php do_action( 'presscore_after_content' ) ?>

<?php get_footer() ?>