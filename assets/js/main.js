/*jslint browser: true*/
/*global jQuery, document, window, location*/
(function ($) {
    "use strict";
    
	jQuery(document).ready(function ($) {


		$(window).on( 'load', function() {
			$('.essb_links_list a').removeAttr('target');
			$('.single-post .wp-block-button a').attr('target', '_blank');
		});


		// CONTACT FORM 7
		
		// error messages for footer form - placed over input in order to prevent 
		// the slide out footer to change it's height and dissapear behind #page 
		$('#grants-contact .wpcf7 .wpcf7-form-control-wrap').on('hover', function () {
			$(this).find('.wpcf7-not-valid-tip').fadeOut(500);
			$(this).find('*').removeClass('wpcf7-not-valid');
		});

		
		// SMOOTH SCROLL

		$('#sidebar .bellows a[href*="#"]:not([href="#"])').click(function () {
			
			if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
			
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			
				if (target.length) {
										
					$('html, body').animate({
					
						scrollTop: target.offset().top - 70
					
					}, 1000);
					
					return false;
				}
			}
		});	


		// STAKEHOLDER TABS
		
		$('.stakeholders a[href*="#"]:not([href="#"])').click(function () {
			
			var target = $(this.hash),
				isTab = $(this).parents('.wpb_tour_tabs_wrapper').length,
				isAccordion = $(this).parents('.stakeholders .vc_tta-container').length,
				scroll = ( ! isTab ) || ( ! isAccordion ) ? false : true;

			if ( isTab ) {
								
				$(target.selector).trigger('click');
				
				$('html, body').animate({
				
					scrollTop: $('.wpb_tour_tabs_wrapper').offset().top - 70
				
				}, 1000);
								
				return false;

				
			} else if ( isAccordion ) {
								
				$( '.wpb-js-composer .vc_tta.vc_general .vc_tta-panel-body' ).slideToggle();
				
			} else {
								
				if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
				
					target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				
					if (target.length) {
											
						$('html, body').animate({
						
							scrollTop: target.offset().top - 70
						
						}, 1000);
						
						return false;
					}
				}
			}
		});	




		// VIDEO TABS
		
		$('.video-submission .ult_tabcontent p a[href*="#"]:not([href="#"])').click(function () {
						
			var target = $(this.hash),
				isTab = $(this).parents('.ult_tabs').length;
				
			if ( isTab ) {
				
				$(target.selector).trigger('click');
				
				$('html, body').animate({
				
					scrollTop: $('.ult_tabs').offset().top - 70
				
				}, 1000);
								
				return false;
			} 
		});	


		
		// SMOOTH SCROLL
			
/*
		$('a[href*="#"]:not([href="#"])').click(function () {
			
			var target = $(this.hash),
				isTab = $(this).parents('.wpb_tour_tabs_wrapper').length,
				isAccordion = $(this).parents('.stakeholders .vc_tta-container').length,
				scroll = ( ! isTab ) || ( ! isAccordion ) ? false : true;

			if ( isTab ) {
								
				$(target.selector).trigger('click');
				
				$('html, body').animate({
				
					scrollTop: $('.wpb_tour_tabs_wrapper').offset().top - 70
				
				}, 1000);
								
				return false;

				
			} else if ( isAccordion ) {
								
				$( '.wpb-js-composer .vc_tta.vc_general .vc_tta-panel-body' ).slideToggle();
				
			} else {
								
				if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
				
					target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				
					if (target.length) {
											
						$('html, body').animate({
						
							scrollTop: target.offset().top - 70
						
						}, 1000);
						
						return false;
					}
				}
			}
		});	
*/	
	
	});
	
}(jQuery));