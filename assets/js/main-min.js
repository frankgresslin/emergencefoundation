/*jslint browser: true*/
/*global jQuery, document, window, location*/
!function(t){"use strict";jQuery(document).ready(function(n){n(window).on("load",function(){n(".essb_links_list a").removeAttr("target"),n(".single-post .wp-block-button a").attr("target","_blank")}),
// CONTACT FORM 7
// error messages for footer form - placed over input in order to prevent 
// the slide out footer to change it's height and dissapear behind #page 
n("#grants-contact .wpcf7 .wpcf7-form-control-wrap").on("hover",function(){n(this).find(".wpcf7-not-valid-tip").fadeOut(500),n(this).find("*").removeClass("wpcf7-not-valid")}),
// SMOOTH SCROLL
n('#sidebar .bellows a[href*="#"]:not([href="#"])').click(function(){if(location.pathname.replace(/^\//,"")===this.pathname.replace(/^\//,"")&&location.hostname===this.hostname){var t=n(this.hash);if((t=t.length?t:n("[name="+this.hash.slice(1)+"]")).length)return n("html, body").animate({scrollTop:t.offset().top-70},1e3),!1}}),
// STAKEHOLDER TABS
n('.stakeholders a[href*="#"]:not([href="#"])').click(function(){var t=n(this.hash),e=n(this).parents(".wpb_tour_tabs_wrapper").length,a=n(this).parents(".stakeholders .vc_tta-container").length,o=!(!e||!a);if(e)return n(t.selector).trigger("click"),n("html, body").animate({scrollTop:n(".wpb_tour_tabs_wrapper").offset().top-70},1e3),!1;if(a)n(".wpb-js-composer .vc_tta.vc_general .vc_tta-panel-body").slideToggle();else if(location.pathname.replace(/^\//,"")===this.pathname.replace(/^\//,"")&&location.hostname===this.hostname&&(t=t.length?t:n("[name="+this.hash.slice(1)+"]")).length)return n("html, body").animate({scrollTop:t.offset().top-70},1e3),!1}),
// VIDEO TABS
n('.video-submission .ult_tabcontent p a[href*="#"]:not([href="#"])').click(function(){var t=n(this.hash),e;if(n(this).parents(".ult_tabs").length)return n(t.selector).trigger("click"),n("html, body").animate({scrollTop:n(".ult_tabs").offset().top-70},1e3),!1})})}(jQuery);