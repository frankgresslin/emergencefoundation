(function( $ ) { 
	"use strict"
	$(function() {
		
		var uniqueNavMenu = $( '#unique_nav_menu' ).attr( 'checked' );
		
		if ( uniqueNavMenu == 'checked') {
			
			$( '#nav_menu_container' ).show();
			
		} else {
			
			$( '#nav_menu_container' ).hide();
			
		}
		
		$( '#unique_nav_menu' ).on( 'change', function() {		
			$( '#nav_menu_container' ).slideToggle();
		});
	
	});
 }( jQuery ));