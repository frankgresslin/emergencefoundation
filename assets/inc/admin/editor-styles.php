<?php

/**
 *  EDITOR STYLES
 *
 *  Contains:
 *  01 - fg_add_editor_styles()
 *  02 - fg_styleselect()
 *  03 - fg_styleselect_formats()
 *    
 *  @package include
 *  @since 	 1.0
 *  @link    https://codex.wordpress.org/Editor_Style
 *  @version 1.0.0
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	ADD STYLESHEET 
 *
 *  @link  https://developer.wordpress.org/reference/functions/add_editor_style/
 */

function fg_add_editor_styles() {
	
    add_editor_style( '/assets/inc/admin/assets/css/custom-editor-styles.css' );
}

add_action( 'after_setup_theme', 'fg_add_editor_styles' );



/**
 * 	SHOW FORMATS DROPDOWN
 *
 *  @param  $buttons  array  tinymce toolbar array
 *  @return $buttons  array  styleselect added
 *
 *  @link  https://goo.gl/vaZdTQ
 */

function fg_styleselect( $buttons ) {
		
	array_unshift( $buttons, 'styleselect' );
	
	return $buttons;
}

add_filter( 'mce_buttons', 'fg_styleselect' );


/**
 * 	ADD STYLES
 *
 *  @param  $init_array  array  tinymce array
 *  @return $init_array  array  style_formats added
 */

function fg_styleselect_formats( $init_array ) {  
	
	$style_formats = array(  
		array(  
			'title' => 'Text Link',  
			'inline' => 'a',  
			'classes' => 'fg-text-link',
			'wrapper' => true,
		),  
		array(  
			'title' => 'Before Post Header',  
			'block' => 'div',
			'classes' => 'fg-intro',
			'wrapper' => true,
		),
		array(  
			'title' => 'Post Title',  
			'block' => 'h1',  
			'classes' => 'fg-post-title',
			'wrapper' => true,
		),
		array(  
			'title' => 'Paragraph',  
			'block' => 'div',  
			'classes' => 'fg-paragraph',
			'wrapper' => true,
		),
		array(  
			'title' => 'Paragraph Header',  
			'block' => 'div',  
			'classes' => 'fg-paragraph-header',
			'wrapper' => true,
		),
		array(  
			'title' => 'Pink Quote',  
			'block' => 'blockquote',  
			'classes' => 'fg-quote',
			'wrapper' => true,
		),
		array(  
			'title' => 'Post Footer',  
			'block' => 'div',  
			'classes' => 'fg-footer',
			'wrapper' => true,
		),
	);  

	$init_array['style_formats'] = json_encode( $style_formats );  
		
	return $init_array;  
} 

add_filter( 'tiny_mce_before_init', 'fg_styleselect_formats' );
