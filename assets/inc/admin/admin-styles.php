<?php

/**
 *  ADMIN SCRIPTS & STYLES
 *
 *  Contains:
 *  01 - fg_admin_styles()
 *
 *  @package include
 *  @since   1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	ENQUEUE STYLESHEET & JAVASCRIPT
 */

function fg_admin_styles() {
	
	$theme_info = wp_get_theme();
		
	wp_enqueue_style( 'fg-admin-styles', INCLUDE_ASSETS_URI . '/inc/admin/assets/css/admin.css', array(), $theme_info->get( 'Version' ) );
	
	wp_register_script( 'fg-admin-scripts', INCLUDE_ASSETS_URI .'/inc/admin/assets/js/admin.js', array( 'jquery' ), $theme_info->get( 'Version' ), true);
	wp_enqueue_script( 'fg-admin-scripts' );
}

add_action('admin_enqueue_scripts', 'fg_admin_styles');
