<?php
/*
Plugin Name: Demo Tax meta class
Plugin URI: https://en.bainternet.info
Description: Tax meta class usage demo
Version: 2.1.0
Author: Bainternet, Ohad Raz
Author URI: https://en.bainternet.info
*/

/* 
 * @URL:	http://www.wpbeginner.com/wp-tutorials/how-to-add-additional-custom-meta-fields-to-custom-taxonomies/
 * @URL:	https://github.com/bainternet/Tax-Meta-Class
 */
 
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


//include the main class file
require_once("Tax-meta-class/Tax-meta-class.php");
if (is_admin()){
  /* 
   * prefix of meta keys, optional
   */
  $prefix = 'fg_';
  /* 
   * configure your meta box
   */
  $config = array(
    'id' => 'demo_meta_box',          // meta box id, unique per meta box
    'title' => 'Demo Meta Box',          // meta box title
    'pages' => array('category', 'project_category'),        // taxonomy name, accept categories, post_tag and custom taxonomies
    'context' => 'normal',            // where the meta box appear: normal (default), advanced, side; optional
    'fields' => array(),            // list of meta fields (can be added by field arrays)
    'local_images' => true,          // Use local or hosted images (meta box images for add/remove)
    'use_with_theme' => true          //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
  );
  
  
  /*
   * Initiate your meta box
   */
  $my_meta =  new Tax_Meta_Class($config);
  
  /*
   * Add fields to your meta box
   */
  
  //text field
//   $my_meta->addText($prefix.'text_field_id',array('name'=> __('My Text ','include'),'desc' => 'this is a field desription'));
  //textarea field
//   $my_meta->addTextarea($prefix.'textarea_field_id',array('name'=> __('My Textarea ','include')));
  //checkbox field
//   $my_meta->addCheckbox($prefix.'checkbox_field_id',array('name'=> __('My Checkbox ','include')));
  //select field
//   $my_meta->addSelect($prefix.'select_field_id',array('selectkey1'=>'Select Value1','selectkey2'=>'Select Value2'),array('name'=> __('My select ','include'), 'std'=> array('selectkey2')));
  //radio field
//   $my_meta->addRadio($prefix.'radio_field_id',array('radiokey1'=>'Radio Value1','radiokey2'=>'Radio Value2'),array('name'=> __('My Radio Filed','include'), 'std'=> array('radionkey2')));
  //date field
//   $my_meta->addDate($prefix.'date_field_id',array('name'=> __('My Date ','include')));
  //Time field
//   $my_meta->addTime($prefix.'time_field_id',array('name'=> __('My Time ','include')));
  //Color field
  $my_meta->addColor($prefix.'color_field_id',array('name'=> __('My Color ','include')));
  //Image field
  $my_meta->addImage($prefix.'image_field_id',array('name'=> __('My Image ','include')));
  //file upload field
//   $my_meta->addFile($prefix.'file_field_id',array('name'=> __('My File ','include')));
  //wysiwyg field
//   $my_meta->addWysiwyg($prefix.'wysiwyg_field_id',array('name'=> __('My wysiwyg Editor ','include')));
  //taxonomy field
//   $my_meta->addTaxonomy($prefix.'taxonomy_field_id',array('taxonomy' => 'category'),array('name'=> __('My Taxonomy ','include')));
  //posts field
//   $my_meta->addPosts($prefix.'posts_field_id',array('args' => array('post_type' => 'page')),array('name'=> __('My Posts ','include')));
  
  /*
   * To Create a reapeater Block first create an array of fields
   * use the same functions as above but add true as a last param
   */
  
  $repeater_fields[] = $my_meta->addText($prefix.'re_text_field_id',array('name'=> __('My Text ','include')),true);
  $repeater_fields[] = $my_meta->addTextarea($prefix.'re_textarea_field_id',array('name'=> __('My Textarea ','include')),true);
  $repeater_fields[] = $my_meta->addCheckbox($prefix.'re_checkbox_field_id',array('name'=> __('My Checkbox ','include')),true);
  $repeater_fields[] = $my_meta->addImage($prefix.'image_field_id',array('name'=> __('My Image ','include')),true);
  
  /*
   * Then just add the fields to the repeater block
   */
  //repeater block
//   $my_meta->addRepeaterBlock($prefix.'re_',array('inline' => true, 'name' => __('This is a Repeater Block','include'),'fields' => $repeater_fields));
  /*
   * Don't Forget to Close up the meta box decleration
   */
  //Finish Meta Box Decleration
  $my_meta->Finish();
}
