<?php

 /**
  *  RESET
  *
  *  Remove unnecessary items from head
  *
  *  Contains:
  *  01 - fg_remove_from_head()
  *  02 - fg_remove_login_generator()
  *  03 - fg_remove_vc_generator()
  *  04 - fg_remove_revslider_meta_tag()
  *  05 - fg_remove_ubermenu_css()
  *  06 - fg_deregister_plugins_css()
  *  07 - fg_remove_emojicons()
  *  08 - fg_hide_admin_toolbar()
  *    
  *  @package include
  *  @since   1.0
  *  @version 1.0.1
  */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	CLEAN HEAD
 *
 *  Remove unwnated elements from <head> section
 */

function fg_remove_from_head() {
	
	/* head */
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'rest_output_link_wp_head' ); // removed /wp-json/ link from head
	
	remove_action( 'wp_head', 'rel_canonical' );  // removes canonical from head
	remove_action( 'wp_head', 'wp_shortlink_wp_head' ); // removes shortlink from head
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		
	/* footer */
	remove_action( 'wp_head', 'wp_oembed_add_host_js' ); // removes wp-embed.min.js from footer
		
	// remove_action( 'wp_head', 'start_post_rel_link' );
	// remove_action( 'wp_head', 'index_rel_link' );
	// remove_action( 'wp_head', 'adjacent_posts_rel_link' );
	
	// remove_action( 'wp_head', 'rest_output_link_wp_head' );
	// remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	
	// add_filter('xmlrpc_enabled', '__return_false');
}

add_action( 'init', 'fg_remove_from_head' );



/**
 * 	CUSTOM LOGIN GENERATOR
 */
    
function fg_start_ob() {
	ob_start( 'fg_remove_login_generator' );
}

function fg_end_ob() {
	ob_end_flush();
}

function fg_remove_login_generator( $output ) {

	if ( defined( 'CUSTOM_LOGIN_VERSION' ) ) {
		
		$target = '<meta name="generator" content="Custom Login v' . CUSTOM_LOGIN_VERSION . '" />';

		$output = str_ireplace($target, '', $output);
		$output = trim($output);
		$output = preg_replace('/^[ \t]*[\r\n]+/m', '', $output);
	}

	return $output;
}

add_action('get_header', 'fg_start_ob');
add_action('wp_head', 'fg_end_ob', 999);



/**
 * 	VISUAL COMPOSER GENERATOR
 */
 
if ( class_exists( 'Vc_Manager' ) ) {
	
    function fg_remove_vc_generator() {
	    
        remove_action( 'wp_head', array( visual_composer(), 'addMetaData' ) );
    }
    
    add_action( 'init', 'fg_remove_vc_generator', 100 );
}


 
 /**
 * 	REVOLUTION SLIDER GENERATOR
 */

function fg_remove_revslider_meta_tag() {
    return '';
}
 
add_filter( 'revslider_meta_generator', 'fg_remove_revslider_meta_tag' );



/**
 * 	UBERMENU STYLES
 *
 *  Styles copied to sass
 */

function fg_remove_ubermenu_css() {
	
    global $uberMenu;
    remove_action( 'wp_head', 'ubermenu_customizer_css' );
    remove_action( 'wp_head', 'ubermenu_inject_custom_css' );
}

add_action( 'wp_loaded' , 'fg_remove_ubermenu_css' );



/**
 * 	DERREGISTER PLUGIN STYLES
 */

function fg_deregister_plugins_css() {
	
	// Yoast
	wp_dequeue_style('yoast-seo-adminbar');
	wp_deregister_style('yoast-seo-adminbar');
	
	// Wordfence
	wp_dequeue_style('wordfenceAJAXcss');
	wp_deregister_style('wordfenceAJAXcss');
	
	// Cookie Law Info - coockie table css
	wp_deregister_style( 'cookielawinfo-table-style' );
	
	// Replace PrettyPhoto with Magnific-Popup
	wp_deregister_script( 'prettyphoto' );
	
}

add_action('wp_enqueue_scripts','fg_deregister_plugins_css');



/**
 * 	SMILEYS
 */

function fg_remove_emojicons() {
    
    // Remove from comment feed and RSS
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    // Remove from Emails
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	
    // Remove from the head, i.e. wp_head()
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

    // Remove from Print-related CSS
    remove_action( 'wp_print_styles', 'print_emoji_styles' );

    // Remove from Admin area
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
}

add_action( 'init', 'fg_remove_emojicons' );



/**
 * 	ADMIN TOOLBAR (hide)
 */

function fg_hide_admin_toolbar() {
	
	if ( ! current_user_can( 'administrator' ) && ! is_admin() ) {
		show_admin_bar( false );
	}
}

add_filter( 'show_admin_bar', 'fg_hide_admin_toolbar' );
