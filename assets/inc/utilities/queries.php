<?php
	
/**
 *  THEME OPTIONS - PRE GET POST QUERIES
 *
 *  Contains:
 *  01 - fg_set_posttype
 *  02 - fg_set_metavalue
 *  03 - fg_pages_in_search_filter()
 *  04 - fg_exclude_pages_from_search()
 *    
 *  @package include
 *  @since 	 1.0
 *  @link    https://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts
 *  @link    https://codex.wordpress.org/Class_Reference/WP_Query
 *  @version 1.0.0
 */
 
 // File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


if ( ! function_exists( 'fg_set_posttype' ) ) :	
	
    /**
     * 	SET POST TYPE 
     *
     *  Loads only specified post type in author template
     *
     * 	@return $query  object	
     */
	
	function fg_set_posttype( $query ) {
		
		if ( ! isset( $_GET['posttype'] ) || is_admin() || ! $query->is_main_query() ) {
			return;
		}
		
		$query->set( 'post_type', array( sanitize_key( $_GET['posttype'] ) ) );
		
		return $query;
	}
	
	add_action( 'pre_get_posts', 'fg_set_posttype');

endif;



if ( ! function_exists( 'fg_set_metavalue' ) ) :	
	
    /**
     * 	Set meta query for main query
     *
     *  Creates archive based on custom meta values
     *
     * 	@return $query  object	
     */
	
	function fg_set_metavalue( $query ) {
		
		if ( ! isset( $_GET['metavalue'] ) || is_admin() || ! $query->is_main_query() ) {
			return;
		}
		
	    $meta_query = $query->get( 'meta_query' );
	    
	    $meta_query[] = array(
	      'key' => sanitize_key( $_GET['metafield'] ),
	      'value' => sanitize_text_field( $_GET['metavalue'] ),
	      'compare' => '=',
	    );
	    
	    $query->set( 'meta_query', $meta_query );
		
		return $query;
	}
	
	add_action( 'pre_get_posts', 'fg_set_metavalue');

endif;



if ( ! function_exists( 'fg_pages_in_search_filter' ) ) :	

	/**
	 * 	SEARCH FILTER 
	 *
	 *  Set post type for search query to enable post type filter for pages
	 *
	 * 	@return  $query  object  the query object
	 *	@link    https://wordpress.stackexchange.com/questions/50828/cant-limit-search-to-only-pages
	 *	@link    https://wordpress.stackexchange.com/questions/52522/adding-meta-key-via-pre-get-posts-causes-navigation-to-disappear
	 */
	
	function fg_pages_in_search_filter( $query ) {	
		
	    if ( isset( $_GET['fg_post_type'] ) && ( $query->is_main_query() )  && ( is_search() ) ) {
		    
		    $query->set( 'post_type', $_GET['fg_post_type'] );
		    $query->set( 'orderby', 'relevance' );
	    }
		
	    return $query;
	}
	
	add_action( 'pre_get_posts','fg_pages_in_search_filter' );

endif;



if ( ! function_exists( 'fg_exclude_pages_from_search' ) ) :	

	/**
	 * 	EXCLUDE PAGES 
	 *
	 *  Exclude pages/posts by id from search results
	 *  Needs to match ajax load more action in functions-plugins.php
	 *
	 * 	@return  $query  object  the query object
	 */
	
	function fg_exclude_pages_from_search( $query ) {
		
	    if ( is_admin() || ! $query->is_main_query() ) {
		    return;
	    }
	        
	    if ( is_search() ) {              
		    $query->set( 'post__not_in', array( 1921, 1427, 2000, 1660, 1664 ) );
	    }
	}
	
	// add_action( 'pre_get_posts', 'fg_exclude_pages_from_search', 1 );

endif;
