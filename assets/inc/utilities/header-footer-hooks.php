<?php

/**
 *  BEFORE & AFTER SITE WRAPPER
 *    
 *  Contains:
 *  01 - fg_before_site_header()
 *  02 - fg_after_footer()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_before_site_header' ) ) :

	/**
	 * 	BEFORE SITE HEADER
	 *
	 *  e.g. skip to content, browser nag, message bars
	 *
	 *  @usedby	header.php
	 */
	
	function fg_before_site_header() {
		
		// $html = '<a id="top"></a>';
		
		// $html .= '<a href="#content" class="skip-link screen-reader-text">' . __( "Skip to content", "include" ) . '</a>';
		
		$html = '<div id="browsehappy">' . __( 'You are using an outdated version of Internet Explorer. Please <a href="//browsehappy.com/" target="_blank" rel="noopener noreferrer">upgrade</a> or <a href="//browsehappy.com/" target="_blank" rel="noopener noreferrer">use a modern browser</a>.', 'include' ) . '<span>x</span></div>';
		
		echo $html;
	}
	
	add_action( 'include_before_site_header', 'fg_before_site_header' );

endif;


