<?php

 /**
  *  WIDGET UTILITIES
  *
  *  Contains:
  *  08 - fg_hide_category_widget_list_items()
  *    
  *  @package include
  *  @since   1.0
  *  @version 1.0.1
  */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	REMOVE UNCATEGORIZED
 *
 *  Remove uncategorized category from default category widget
 *
 *	@param   $category_args  array  the category args
 *  @return  $category_args  array  mereged array
 *  
 */

function fg_hide_category_widget_list_items( $category_args ) {
  
    $category_args = array_merge( $category_args, array( 'exclude' => 1 ) );

    return $category_args;
}

add_filter( 'widget_categories_args', 'fg_hide_category_widget_list_items' );
