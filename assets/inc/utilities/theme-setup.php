<?php

/**
 *  THEME SETUP
 *
 *  Contains:
 *  01 - fg_theme_setup()
 *  02 - fg_scripts_and_styles()
 *  03 - fg_add_to_head()
 *  04 - fg_login_styles()
 *  05 - fg_robots_txt()
 *
 *  @package include
 *  @since   1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * CONTENT WIDTH
 */
 
if ( ! isset( $content_width ) ) { $content_width = 1140; }


/**
 * 	THEME SETUP
 */

function fg_theme_setup() {
	
	// THEME SUPPORT
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails', array( 'post' ) );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', ) );
	add_theme_support( 'automatic-feed-links' );
	
	// keep theme check happy for now...
	// add_theme_support( 'custom-header' );
	// add_theme_support( 'custom-background' );
	
	// POST TYPE SUPPORT
	add_post_type_support( 'page', 'excerpt' );
		
		
	// TEXT DOMAIN
	load_theme_textdomain ( 'include', get_template_directory() . '/assets/languages' );
	
	
	// IMAGE SIZES
	add_image_size( 'fg-blog-thumbnail', 300, 300, true );	
	
	
	// NAVIGATION MENUS
	register_nav_menus( array( 
		'primary' 		=> __( 'Primary', 'include' ), 
		'top_bar' 		=> __( 'Top Bar', 'include' ),
		'bottom_bar' 	=> __( 'Bottom Bar', 'include' ) 
	));
	
	
	// WOOCOMMERCE
	// add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'fg_theme_setup' );



/**
 * 	ENQUEUE SCRIPTS & STYLES
 */

function fg_scripts_and_styles() {
	
	if ( is_singular() ) wp_enqueue_script( "comment-reply" );
	
	if ( ! is_admin() && ! in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {
		
		$theme_info = wp_get_theme();
		
		/* Load CSS */
		wp_enqueue_style( 'include-stylesheet', INCLUDE_ASSETS_URI . '/css/include.min.css', array(), $theme_info->get( 'Version' ) );
		
		/* Load JS */
		wp_register_script( 'include-scripts', INCLUDE_ASSETS_URI .'/js/main.min.js', array( 'jquery' ), $theme_info->get( 'Version' ), true );
		wp_enqueue_script( 'include-scripts' );	
		
		
		// Replace PrettyPhoto with PhotoSwipe
		// wp_enqueue_style( 'photoswipe-css', INCLUDE_ASSETS_URI . '/css/plugins/photoswipe/photoswipe.css', array(), $theme_info->get( 'Version' ) );
		// wp_enqueue_style( 'photoswipe-skin-css', INCLUDE_ASSETS_URI . '/css/plugins/photoswipe/default-skin/default-skin.css', array(), $theme_info->get( 'Version' ) );
		
		// wp_register_script( 'photoswipe-scripts', INCLUDE_ASSETS_URI . '/js/plugins/photoswipe/photoswipe.min.js', array( 'jquery' ), $theme_info->get( 'Version' ), false );
		// wp_register_script( 'photoswipe-ui-scripts', INCLUDE_ASSETS_URI . '/js/plugins/photoswipe/photoswipe-ui-default.min.js', array( 'jquery' ), $theme_info->get( 'Version' ), false );
		
		// wp_enqueue_script( 'photoswipe-scripts' );	
		// wp_enqueue_script( 'photoswipe-ui-scripts' );
	
		/* Load Ajax */
		// Use the Nonce only for DB updates e.g. Form Submits etc.	
		// wp_enqueue_script( 'ajax-script', INCLUDE_ASSETS_URI . '/assets/js/ajax/ajax.js', array('jquery'), $theme_info->get( 'Version' ), true );
		// wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php'), 'ajax_nonce' => wp_create_nonce( 'myajax-nonce' ) ) );	
		
	}
}

add_action('wp_enqueue_scripts', 'fg_scripts_and_styles');



/**
 * 	FAVICONS
 */

function fg_add_to_head() { ?>
 
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo INCLUDE_ASSETS_URI ?>/assets/images/favicons/apple-touch-icon.png?v=jw67982XoB">
<link rel="icon" type="image/png" href="<?php echo INCLUDE_ASSETS_URI ?>/assets/images/favicons/favicon-32x32.png?v=jw67982XoB" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo INCLUDE_ASSETS_URI ?>/assets/images/favicons/favicon-16x16.png?v=jw67982XoB" sizes="16x16">
<link rel="manifest" href="<?php echo INCLUDE_ASSETS_URI ?>/assets/images/favicons/manifest.json?v=jw67982XoB">
<link rel="mask-icon" href="<?php echo INCLUDE_ASSETS_URI ?>/assets/images/favicons/safari-pinned-tab.svg?v=jw67982XoB" color="#000000">
<link rel="shortcut icon" href="<?php echo INCLUDE_ASSETS_URI ?>/assets/images/favicons/favicon.ico?v=eE5z8xKdq5">
<meta name="theme-color" content="#ffffff"> 
    
<?php }
	    
add_action( 'wp_head', 'fg_add_to_head' );



/**
 * 	ENQUEUE LOGIN PAGE STYLESHEET
 */
 
function fg_login_styles() {
	
	$theme_info = wp_get_theme();
	wp_enqueue_style( 'login-stylesheet', INCLUDE_ASSETS_URI . '/css/login.css', array('login'), $theme_info->get( 'Version' ) );
}

// add_action( 'login_enqueue_scripts', 'fg_login_styles' );



/**
 * 	ROBOTS.TXT
 */

function fg_robots_txt( $output, $public ) {
	
	$site_url = home_url();

    $output = "User-agent: *\n";
    $output .= "Disallow: /readme.html\n";
	$output .= "Disallow: /wp-admin/\n";
	$output .= "Allow: /wp-admin/admin-ajax.php\n";
	$output .= "Disallow: /wp-includes/\n";
	$output .= "Allow: /wp-includes/js/\n";
	$output .= "Allow: /wp-includes/images/\n";
	$output .= "Disallow: /trackback/\n";
	$output .= "Disallow: /wp-login.php\n";
	$output .= "Disallow: /wp-register.php\n";
	$output .= "Disallow: /assets/infinitewp/\n";
    $output .= "Disallow: /assets/wflogs/\n\n";
    
    $output .= "Sitemap: $site_url/sitemap_index.xml";
    
    return $output;
}

add_filter( 'robots_txt', 'fg_robots_txt', 10, 2 );
