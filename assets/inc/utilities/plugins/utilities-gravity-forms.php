<?php

 /**
  *  PLUGIN FUNCTIONS - GRAVITY FORMS
  *
  *  Contains:
  *  01 - gform_confirmation_anchor
  *    
  *  @package include
  *  @since   1.0
  *  @version 1.0.0
  */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


	if ( function_exists( 'gravity_form' ) ) :	
		
	/**
	 * 	GRAVITY FORMS: CONFIRMATION ANCHOR
	 *
	 *  @link  https://www.gravityhelp.com/documentation/article/gform_confirmation_anchor/
	 */
	
	// add_filter( 'gform_confirmation_anchor', function() { return 100; });
	// add_filter( 'gform_confirmation_anchor', '__return_false' );
	
	
	add_filter( 'gform_confirmation_anchor', function() {
	    return 20;
	} );

endif;

