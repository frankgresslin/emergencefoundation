<?php

 /**
  *  PLUGIN FUNCTIONS - RELEVANSSI
  *
  *  Contains:
  *  01 - relevanssi_pre_excerpt_content
  *    
  *  @package include
  *  @since   1.0
  *  @version 1.0.0
  */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	REMOVE SHORTCODES 
 *
 *  Remove unprocessed Visual Composer Shortcodes from Relevanssi
 *
 * 	@return  $content  string  the content
 *	@link    https://www.relevanssi.com/user-manual/filter-hooks/
 *	@link    https://wordpress.org/support/topic/visual-composer-shortcodes-in-search-results-2/
 *	@link    https://gist.github.com/yratof/8e1cb96c5bdc14e439ad5cadde263b98
 */

function fg_remove_vc_shortcodes( $content ) {
	
	// Visual Composer shortcodes
    $content = preg_replace('/\[\/?vc_.*?\]/', '', $content);
    $content = preg_replace('/\[\/?rev_slider_vc.*?\]/', '', $content);
    
    // remove a string
    // $content = str_replace('Some String', ' ', $content);
    
    return $content;
}

add_filter('relevanssi_pre_excerpt_content', 'fg_remove_vc_shortcodes');
