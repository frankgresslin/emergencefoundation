<?php

 /**
  *  PLUGIN FUNCTIONS - THEME MY LOGIN
  *    
  *  @package include
  *  @since   1.0
  *  @version 1.0.0
  */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


if ( function_exists( 'theme_my_login' ) ) :	
	
    add_filter( 'tml_title', function() { return false; }, '' );

endif;
