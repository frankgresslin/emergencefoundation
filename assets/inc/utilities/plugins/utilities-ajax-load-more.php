<?php

 /**
  *  PLUGIN FUNCTIONS - AJAX LOAD MORE
  *
  *  Contains:
  *  01 - alm_query_args_relevanssi
  *  02 - alm_query_args_author_archive
  *    
  *  @package include
  *  @since   1.0
  *  @version 1.0.0
  */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	REELVANSSI FOR AJAX LOAD MORE
 *
 *  @param  $args  array 
 *  @return $args  array 
 *
 *  @link  https://connekthq.com/plugins/ajax-load-more/extensions/relevanssi/#how-it-works
 *  @link  https://connekthq.com/plugins/ajax-load-more/docs/filter-hooks/#alm_query_args
 */

function fg_alm_query_args_relevanssi( $args ) {
	
	echo "<pre>" . print_r( $args, true ) . "</pre>";
	
   $args = apply_filters( 'alm_relevanssi', $args );
   
   return $args;
}

// add_filter( 'alm_query_args_relevanssi', 'fg_alm_query_args_relevanssi');



/**
 * 	AJAX LOAD MORE - AUTHOR ARCHIVE
 *
 *  Needs to match pre_get_posts in functions-customise.php
 *
 *  @param  $args  array 
 *  @return $args  array 
 *
 *  @link  https://connekthq.com/plugins/ajax-load-more/docs/filter-hooks/#alm_query_args
 */

function fg_alm_author_archive_args( $args, $page ) {
   	 
   	$args['orderby'] = 'post_type'; 
   	$args['order'] = 'ASC'; 
	$args['tax_query'] = array(
		array(
			'taxonomy' => 'post_format',
			'field' => 'slug',
			'terms' => array('post-format-video', 'post-format-status', 'post-format-quote'),
			'operator' => 'NOT IN',
		)
	);
   
   return $args;
}

// add_filter( 'alm_query_args_author_archive', 'fg_alm_author_archive_args', 10, 2);
