<?php

/**
 *  THEME OPTIONS - BODY CLASS
 *
 *  Contains:
 *  01 - fg_add_theme_options_as_body_class()
 *  02 - fg_add_post_slug_as_body_class()
 *  03 - fg_add_categories_as_body_class()
 *  04 - fg_add_current_blog_slug_as_body_class()
 *  05 - fg_add_query_string_as_body_class()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * 	THEME OPTIONS 
 *
 *  e.g. for sticky navigation or slide out footer
 */

function fg_add_theme_options_as_body_class( $classes ) {
		
	if ( ! is_admin() ) {
		
		// set primary menu type set in theme customizer or page options		
		$classes[] .= fg_get_navigation_type();
						
		if ( ! is_404() ) {
			
			// set footer type set in theme customizer
			// set default footer on 404
			$classes[] .= fg_get_option( 'footer_type' );
			
		}
				
		// set sidebar
		$classes[] .= ( fg_sidebar_left( false ) ) ? 'sidebar-left' : false;
		$classes[] .= ( fg_sidebar_right( false ) ) ? 'sidebar-right' : false;
		$classes[] .= ( ( ! fg_sidebar_left( false ) ) && ( ! fg_sidebar_right( false ) ) ) ? 'sidebar-none' : false;
		
	}
		
	return $classes;
}

// add_filter('body_class','fg_add_theme_options_as_body_class');



/**
 * 	PAGE / POST SLUG 
 *
 *  for page specific styling
 */

function fg_add_post_slug_as_body_class( $classes ) {
	
	$classes[] = ( is_single() || is_page() ) ? fg_the_slug( false ) : '';
	
	return $classes;
}

add_filter('body_class','fg_add_post_slug_as_body_class');



/**
 * 	PARENT CATEGORY 
 *
 *  styling based on category ( e.g. care product sections )
 */

function fg_add_categories_as_body_class( $classes ) {

	if ( is_single() ) {
	
		$categories = get_the_category( get_the_ID() );
		
		foreach ( $categories as $category ) {
			
			// add category if it is top level
			if ( $category->category_parent == 0 ) {
				
				$classes[] .= $category->slug;
				
			} else {
				
				// get the parent if it's a child category
				$category_parent = get_term( $category->category_parent, 'category' );
				
				$classes[] .= $category_parent->slug;
				
			}
		}
	}
	
	return $classes;
}

// add_filter('body_class','fg_add_categories_as_body_class');



/**
 * 	MULTI SITE SLUG 
 *
 *  style components depeding on what site is viewd
 */

function fg_add_current_blog_slug_as_body_class( $classes ) {

	if ( is_multisite() ) {
	
		$current_blog_id = get_current_blog_id();
		$blog_details = get_blog_details( $current_blog_id );
		
		$blog_slug = explode( '/', $blog_details->siteurl );
		$blog_slug = $blog_slug[ sizeof( $blog_slug ) -1 ];
		
		$classes[] .= $blog_slug;
	}
	
	return $classes;
}

// add_filter('body_class','fg_add_current_blog_slug_as_body_class');



/**
 * 	QUERY STRING 
 *
 *  allows styling of pages based on query string ( e.g. care )
 */

if ( isset( $_GET['add_query_sting'] ) ) {
 	
 	function fg_add_query_string_as_body_class( $classes ) {
	
		$classes[]  .= 'add_query_sting';
		
		return $classes;
	}
	
	// add_filter('body_class','fg_add_query_string_as_body_class');
}
