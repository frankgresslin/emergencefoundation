<?php

/**
 *  THEME OPTIONS - UTILITIES
 *
 *  Contains:
 *  01 - fg_get_id()
 *  02 - fg_save_meta_box_check()
 *  03 - fg_the_slug()
 *  04 - inc_get_ancestor_id()
 *   
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	ID FOR ARCHIVES
 *
 *  Set id for templates that don't have an own WordPress page (archives, search, single, 404 and home)
 *
 *  @usedby	 options-page-header.php
 *  @usedby	 options-page.php
 *  @usedby	 options-sidebars.php
 *  @usedby	 options-site-header.php
 */

function fg_get_id() {
	return ( is_home() || is_archive() || is_single() || is_404() || is_search() ) ? get_option( 'page_for_posts' ) : get_the_ID();
}



/**
 * 	META BOX CHECK
 *
 *  Check wether meta box values can be saved
 *
 *  @usedby	 meta-boxes-post.php
 *  @usedby	 meta-boxes-page.php
 */

function fg_save_meta_box_check( $post_id ) {
				
	// check post revision and autosave
	if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
		return false;
	}
	
	// check user capabilities	
	if ( ( 'page' == $_POST[ 'post_type' ] ) && ( ! current_user_can( 'edit_page', $post_id ) ) ) {
		
		return false;
		
	} else {
		
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return false;
		}
	}
	
	return true;
}



/**
 * 	POST SLUG 
 *
 *  echo or return the post slug
 *
 *  @param   bool  $echo
 *  @return  string  $slug  - the post slug
 *
 *  @usedby	body-class.php
 */

function fg_the_slug( $echo = true  ) {
				
	if ( $echo ) { echo basename( get_permalink() ); }
		
	return basename( get_permalink() );
}



if ( ! function_exists( 'inc_get_ancestor_id' ) ) :

	/**
	 * 	ANCESTOR ID 
	 *
	 *  return the id of the page ancestor
	 *
	 *  @return  string
	 *
	 */
	
	function inc_get_ancestor_id() {
		
		global $post;
		
		if ( $post->post_parent ) {
			
			$ancestors = get_post_ancestors( $post->ID );
			$root = count( $ancestors ) -1;
			return $ancestors[ $root ];
			
		} else {
			
			return $post->ID;
			
		}
	}

endif;
