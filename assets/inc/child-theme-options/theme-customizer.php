<?php

/**
 *  THEME CUSTOMIZER
 *
 *  Contains:
 *  01 - fg_rss_section()
 *  02 - fg_site_logo()
 *  03 - fg_analytics_section()
 *  04 - fg_project_options_section()
 *
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


function fg_customize_register( $wp_customize ) {

	/*-----------------------------------------------------------------------------------*/
	/*	REMOVE DEFAULT SECTIONS
	/*-----------------------------------------------------------------------------------*/
	
	$wp_customize->remove_section( 'themes' );
	$wp_customize->remove_section( 'title_tagline' );
// 	$wp_customize->remove_section( 'nav' );
// 	$wp_customize->remove_panel( 'nav_menus' );
	$wp_customize->remove_section( 'static_front_page' );
// 	$wp_customize->remove_panel( 'widgets' );
	

	/*-----------------------------------------------------------------------------------*/
	/*	SANITIZATION FUNCTIONS
	/*-----------------------------------------------------------------------------------*/

	function fg_sanitize_checkbox( $value ) {
		return ( isset( $value ) && true == $value ) ? true : false;
	}
	
	
	function fg_sanitize_multi_checkbox( $values ) {
	
	    $multi_values = !is_array( $values ) ? explode( ',', $values ) : $values;
	
	    return !empty( $multi_values ) ? array_map( 'sanitize_text_field', $multi_values ) : array();
	}


	function fg_sanitize_select( $input, $setting ) {
	
		$input = sanitize_key( $input );
	
		$choices = $setting->manager->get_control( $setting->id )->choices;
	
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
	}
	
	
	function fg_sanitize_image( $input ) {
 
	    $output = '';
	 
	    $filetype = wp_check_filetype( $input );
	    $mime_type = $filetype['type'];
	 
	    if ( strpos( $mime_type, 'image' ) !== false ) {
	        $output = $input;
	    }
 
		return $output;
	}
	
	
	function fg_sanitize_fileupload( $input ) {
 
	    $output = '';
	 
	    $filetype = wp_check_filetype( $input );
	    $mime_type = $filetype['type'];
	 
	    if ( strpos( $mime_type, 'pdf' ) !== false ) {
	        $output = $input;
	    }
 
		return $output;
	}
	
	
	function fg_sanitize_textarea( $input ) {
		return wp_kses_post( $input );
	}


	/*-----------------------------------------------------------------------------------*/
	/*	TEXTAREA CONTROL CLASS
	/*-----------------------------------------------------------------------------------*/
	
	if ( class_exists( 'WP_Customize_Control' ) ) {
		
	    # Adds textarea support to the theme customizer
	    
	    class FGTextAreaControl extends WP_Customize_Control {
		    
	        public $type = 'textarea';
	       
	        public function __construct( $manager, $id, $args = array() ) {
		        
	            $this->statuses = array( '' => __( 'Default', 'include' ) );
	            
	            parent::__construct( $manager, $id, $args );
	        }
	 
	        public function render_content() {
		        
	            echo '<label>
	                	<span class="customize-control-title">' . esc_html( $this->label ) . '</span>
						<textarea rows="10" style="width:100%;" ';
						
						$this->link();
						echo '>' .  esc_attr( $this->value() )  . '</textarea>
						
	                </label>';
	        }
	    }
	}

	
	
	
	/*-----------------------------------------------------------------------------------*/
	/*	SECTION: SITE ICON
	/*-----------------------------------------------------------------------------------*/
   
	$wp_customize->add_section( 'fg_site_header_section' , array(
		'title'      			=> __( 'Site Logo', 'include' ),
		'priority'   			=> 210
	));
	


		
	/* logo upload */
	
	$wp_customize->add_setting( 'fg_options[site_logo]', array(
	    'default'				=> 'image.jpg',
	    'capability'			=> 'edit_theme_options',
	    'type'					=> 'option',
	    'sanitize_callback' 	=> 'fg_sanitize_image',
	));
	 
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'fg_options[site_logo]', array(
	    'label'    				=> __( 'Site Icon', 'include' ),
	    'settings' 				=> 'fg_options[site_logo]',
	    'section'  				=> 'fg_site_header_section',
	)));
	
	
	
	/*-----------------------------------------------------------------------------------*/
	/*	SECTION: GOOGLE ANALYTICS
	/*-----------------------------------------------------------------------------------*/		
	    
	$wp_customize->add_section( 'fg_analytics_section' , array(
		'title'      			=> __( 'Google Analytics', 'include' ),
		'priority'   			=> 250
	));
	
	 
	$wp_customize->add_setting( 'fg_options[google_analytics]', array(
	    'capability'     		=> 'edit_theme_options',
	    'type'           		=> 'option',
	    'sanitize_callback' 	=> 'fg_sanitize_textarea',
	));
	 
	$wp_customize->add_control( 'google_analytics', array(
	    'label'      			=> __( 'Google Analytics', 'include' ),
	    'section'    			=> 'fg_analytics_section',
	    'type'    				=> 'textarea',
	    'settings'   			=> 'fg_options[google_analytics]',
	));
	
	

	/*-----------------------------------------------------------------------------------*/
	/*	SECTION: PROJECT OPTIONS
	/*-----------------------------------------------------------------------------------*/		
	    
	$wp_customize->add_section( 'fg_project_options_section' , array(
		'title'      			=> __( 'Project Options', 'include' ),
		'priority'   			=> 260
	));
	
	 
	/**
	 * TEXT INPUT
	 */

	$wp_customize->add_setting( 'fg_options[text_id]', array(
		'default'       		=> 'placeholder text',
	    'capability' 			=> 'edit_theme_options',
	    'type'       			=> 'option',
	    'sanitize_callback' 	=> 'sanitize_text_field',
	));
	 
	$wp_customize->add_control( 'fg_options[text_id]', array(
		'type' 					=> 'text',
        'label' 				=> 'Text Input Label',
        'section' 				=> 'fg_project_options_section',
	));
	
	
	
	
	/**
	 * RADIO CONTROLS
	 */
	 
	$wp_customize->add_setting( 'fg_options[radio_id]', array(
	    'default'      			=> 'value2',
	    'capability'  		   	=> 'edit_theme_options',
	    'type'    		       	=> 'option',
	    'sanitize_callback' 	=> 'fg_sanitize_checkbox',
	));
	 
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fg_options[radio_id]', array(
	    'type'  		     	=> 'radio',
	    'label'      			=> __( 'Radio Label', 'include' ),
	    'section' 		   		=> 'fg_project_options_section',
	    'settings'   			=> 'fg_options[radio_id]',
	    'choices'   		 	=> array(
	        'value1'		 	=> __( 'Choice 1', 'include' ),
	        'value2'		 	=> __( 'Choice 2', 'include' ),
	        'value3' 			=> __( 'Choice 3', 'include' ),
	    ),
	)));
	
	
	/**
	 * CHECKBOX
	 */
	 
	$wp_customize->add_setting( 'fg_options[checkbox_id]', array(
		'default'     			=> '1',
		'type'       			=> 'option',
		'capability' 			=> 'edit_theme_options',
		'sanitize_callback' 	=> 'fg_sanitize_checkbox',
	));
	 
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fg_options[checkbox_id]', array(
	    'type'   		  		=> 'checkbox',
	    'label'   		 		=> __( 'Checkbox Label', 'include' ),
	    'section'  				=> 'fg_project_options_section',
	    'settings' 				=> 'fg_options[checkbox_id]',
	    'std'					=> '1'
	)));	


	/**
	 * SELECT
	 */
	 
	$wp_customize->add_setting( 'fg_options[select_id]', array(
	    'default' 		       	=> 'value2',
	    'capability'    		=> 'edit_theme_options',
	    'type'           		=> 'option',
	    'sanitize_callback' 	=> 'fg_sanitize_select',
	 
	));
	
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'fg_options[select_id]', array(
	    'type'    				=> 'select',
	    'label'  		 		=> __( 'Select Label:', 'include' ),
	    'section' 				=> 'fg_project_options_section',
	    'settings' 				=> 'fg_options[select_id]',
	    'choices'   		 	=> array(
	        'value1'		 	=> __( 'Choice 1', 'include' ),
	        'value2' 			=> __( 'Choice 2', 'include' ),
	        'value3' 			=> __( 'Choice 3', 'include' ),
	    ),
	)));
		
	
	/**
	 * IMAGE UPLOAD
	 */
	 
	$wp_customize->add_setting( 'fg_options[image_upload_id]', array(
	    'default'				=> 'image.jpg',
	    'capability'			=> 'edit_theme_options',
	    'type'					=> 'option',
	    'sanitize_callback' 	=> 'fg_sanitize_image',
	));
	 
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'fg_options[image_upload_id]', array(
	    'label'    				=> __( 'Image Upload Label', 'include' ),
	    'section'  				=> 'fg_project_options_section',
	    'settings' 				=> 'fg_options[image_upload_id]',
	)));
	
	

	
		
	/**
	 * FILE UPLOAD
	 */
	 
	$wp_customize->add_setting( 'fg_options[upload_id]', array(
	    'default'				=> 'upload',
	    'capability'			=> 'edit_theme_options',
	    'type'					=> 'option',
	    'sanitize_callback' 	=> 'fg_sanitize_fileupload',
	 
	));
	 
	$wp_customize->add_control( new WP_Customize_Upload_Control( $wp_customize, 'upload_id', array(
	    'label'    				=> __( 'File Upload Label', 'include' ),
	    'section'  				=> 'fg_project_options_section',
	    'settings' 				=> 'fg_options[upload_id]',
	)));
		
	
	/**
	 * COLOR PICKER
	 */
	 
	$wp_customize->add_setting( 'fg_options[color_picker_id]', array(
	    'default'           	=> '000',
	    'capability'        	=> 'edit_theme_options',
	    'type'           		=> 'option',
	    'sanitize_callback' 	=> 'sanitize_hex_color',
	));
	 
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_picker_id', array(
	    'label'    				=> __( 'Color Picker Label', 'include' ),
	    'section'  				=> 'fg_project_options_section',
	    'settings' 				=> 'fg_options[color_picker_id]',
	)));
		
	
	/**
	 * PAGES DROP-DOWN
	 */
	 
	$wp_customize->add_setting( 'fg_options[page_dropdown_id]', array(
	    'capability'     		=> 'edit_theme_options',
	    'type'           		=> 'option',
		'sanitize_callback' 	=> 'fg_sanitize_select',
	));
	 
	$wp_customize->add_control( 'page_dropdown_id', array(
	    'label'      			=> __('Page Drop-Down Label', 'include'),
	    'section'    			=> 'fg_project_options_section',
	    'type'    				=> 'dropdown-pages',
	    'settings'   			=> 'fg_options[page_dropdown_id]',
	));
	
	
	/**
	 * TEXTAREA
	 */
	 
	$wp_customize->add_setting( 'fg_options[textarea_id]', array(
	    'capability'     		=> 'edit_theme_options',
	    'type'           		=> 'option',
	    'sanitize_callback' 	=> 'fg_sanitize_textarea',
	));
	 
	$wp_customize->add_control( 'textarea_id', array(
	    'label'      			=> __( 'Textarea Label', 'include' ),
	    'section'    			=> 'fg_project_options_section',
	    'type'    				=> 'textarea',
	    'settings'   			=> 'fg_options[textarea_id]',
	));
}

add_action( 'customize_register', 'fg_customize_register' );



/*-----------------------------------------------------------------------------------*/
/*	GET THEME OPTIONS
/*-----------------------------------------------------------------------------------*/
 
function fg_get_option( $name, $default = false ) {
   
    $options = ( get_option( 'fg_options' ) ) ? get_option( 'fg_options' ) : null;
                
    if ( isset( $options[ $name ] ) ) {
        return apply_filters( 'fg_options_$name', $options[ $name ] );
    }
    
    return apply_filters( 'fg_options_$name', $default );
}