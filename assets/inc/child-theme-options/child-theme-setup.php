<?php

/**
 *  FUNCTIONS
 *
 *  Any core and theme customisation funcions
 *
 *  @package include
 *  @since   1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


function fg_theme_setup() {
	
	add_post_type_support( 'page', 'excerpt' );
	
	add_image_size( 'fg-blog-thumbnail', 300, 300, true );	

}

// add_action( 'after_setup_theme', 'fg_theme_setup' );



/*-----------------------------------------------------------------------------------*/
/*	ENQUEUE THEME STYLESHEETS
/*-----------------------------------------------------------------------------------*/

function fg_scripts() {
	
	if ( is_singular() ) wp_enqueue_script( "comment-reply" );
	
	if ( ! is_admin() && ! in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) ) ) {
		
		$theme_info = wp_get_theme();
		
		/* Load CSS */
		
		wp_enqueue_style( 'include-child-stylesheet', get_stylesheet_directory_uri() . '/assets/css/include-child.min.css', array(), $theme_info->get( 'Version' ) );


		/* Load JS */
		
		// The7 main.js - derregister parent script & fix slide out footer bug
		// wp_deregister_script( 'dt-main' );
		// wp_register_script( 'the7-scripts', get_stylesheet_directory_uri() . '/assets/js/override/the7-main.min.js', array( 'jquery' ), $theme_info->get( 'Version' ), true );
		// wp_enqueue_script( 'the7-scripts' );

		wp_register_script( 'include-scripts', INCLUDE_ASSETS_URI . '/js/main-min.js', array( 'jquery' ), $theme_info->get( 'Version' ), true );
		wp_enqueue_script( 'include-scripts' );
		
		// Magnific-Popup
		// wp_enqueue_style( 'magnific-popup', INCLUDE_ASSETS_URI . '/css/plugins/magnific-popup.css', array(), $theme_info->get( 'Version' ) );
		// wp_register_script( 'magnific-popup-scripts', INCLUDE_ASSETS_URI . '/js/plugins/jquery.magnific-popup.min.js', array( 'jquery' ), $theme_info->get( 'Version' ), false );
		// wp_enqueue_script( 'magnific-popup-scripts' );	

		/* Load Ajax */
		// Use the Nonce only for DB updates e.g. Form Submits etc.	
		// wp_enqueue_script( 'ajax-script', INCLUDE_ASSETS_URI . '/js/ajax/ajax.js', array('jquery'), $theme_info->get( 'Version' ), true );
		// wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php'), 'ajax_nonce' => wp_create_nonce( 'myajax-nonce' ) ) );
	}
}

add_action('wp_enqueue_scripts', 'fg_scripts', 99);



/*-----------------------------------------------------------------------------------*/
/*	ENQUEUE LOGIN PAGE STYLESHEETS 
/*-----------------------------------------------------------------------------------*/


function fg_login_styles() {
	
	$theme_info = wp_get_theme();
	wp_enqueue_style( 'login-stylesheet', INCLUDE_ASSETS_URI . '/css/login.css', array('login'), $theme_info->get( 'Version' ) );
}

// add_action( 'login_enqueue_scripts', 'fg_login_styles' );




/**
 * 	FAVICONS
 */

function fg_add_to_head() { ?>

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo INCLUDE_ASSETS_URI ?>/images/favicons/apple-touch-icon.png?v=pgqo7jMKEw">
<link rel="icon" type="image/png" href="<?php echo INCLUDE_ASSETS_URI ?>/images/favicons/favicon-32x32.png?v=pgqo7jMKEw" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo INCLUDE_ASSETS_URI ?>/images/favicons/favicon-16x16.png?v=pgqo7jMKEw" sizes="16x16">
<link rel="manifest" href="<?php echo INCLUDE_ASSETS_URI ?>/images/favicons/manifest.json?v=pgqo7jMKEw">
<link rel="mask-icon" href="<?php echo INCLUDE_ASSETS_URI ?>/images/favicons/safari-pinned-tab.svg?v=pgqo7jMKEw" color="#f35142">
<link rel="shortcut icon" href="<?php echo INCLUDE_ASSETS_URI ?>/images/favicons/favicon.ico?v=pgqo7jMKEw">
<meta name="theme-color" content="#ffffff"> 
    
<?php }
	    
add_action( 'wp_head', 'fg_add_to_head' );
