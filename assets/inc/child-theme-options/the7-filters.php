<?php

/**
 *  THEME OPTIONS - CORE
 *
 *  Contains:
 *  01 - presscore_config_post_id_filter
 *  02 - presscore_page_title
 *  03 - presscore_main_container_classes
 *  04 - presscore_post_type_dt_portfolio_args
 *  05 - presscore_taxonomy_dt_portfolio_category_args
 *     
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


/**
 * 	SET ID
 */

function fg_config_post_id_filter( $classes ) {
	return ( is_home() || is_archive() || is_404() || is_search() ) ? get_option( 'page_for_posts' ) : get_the_ID();
}

add_filter( 'presscore_config_post_id_filter', 'fg_config_post_id_filter' );



/**
 * 	PAGE TITLE
 *
 *  Filters the7 page titles
 */

function fg_page_title( $title ) {
	
	if ( is_home() ) {
		return $title;
	}
	
	elseif ( is_search() ) {
		return 'Search Results for: <span>' . get_search_query() . '</span>';
	}
	
	elseif ( is_archive() ) {
		return get_the_archive_title();;
	}
	
	else {
		return $title;
	}
}

add_filter( 'presscore_page_title', 'fg_page_title' );



/**
 * 	SIDEBARS
 *
 *  Set the sidebar for any template
 */

function fg_main_container_classes( $classes ) {
	
	if ( is_author() ) {
		
		return 'sidebar-none';
		
	} elseif ( is_search() ) {
		
		return 'sidebar-none';
		
	} elseif ( is_singular( 'post' ) ) {
		
		return 'sidebar-none';

	} elseif ( is_singular( 'project' ) ) {
		
		return 'sidebar-none';
		
	} elseif ( is_singular( 'vimeo-video' ) ) {
		
		return 'sidebar-none';
		
	}  elseif ( is_post_type_archive( 'project' ) ) {
		
		return 'sidebar-right';
		
	} elseif ( 'ui' == get_post_type() ) {
		
		return 'sidebar-none';
		
	} elseif ( is_home() || is_archive() ) {
		
		$classes = array();
		$meta = get_post_meta( get_option( 'page_for_posts' ) );
		$sidebar = ( isset( $meta['_dt_sidebar_position'][0] ) ) ? $meta['_dt_sidebar_position'][0] : 'disabled';
		
		switch( $sidebar ) {
			case 'left':
				$classes[] = 'sidebar-left';
				break;
			case 'disabled':
				$classes[] = 'sidebar-none';
				break;
			default :
				$classes[] = 'sidebar-right';
		}
		
	} elseif ( is_404() ) {
		
		return 'sidebar-none';
		
	}  else {
		
		return $classes;
	}
	
	return $classes;
}

add_filter( 'presscore_main_container_classes', 'fg_main_container_classes' );





/**
 * 	PORTFOLIO ARGS
 *
 *  Update the labels for the Posrtfolio Custom Post Type
 */

function fg_dt_portfolio_args() {
	
	return array(
			'labels'                => array(
				'name'                  => _x( 'Projects',               'backend portfolio', 'dt-the7-core' ),
				'singular_name'         => _x( 'Project',      			 'backend portfolio', 'dt-the7-core' ),
				'add_new'               => _x( 'Add New',                'backend portfolio', 'dt-the7-core' ),
				'add_new_item'          => _x( 'Add New Item',           'backend portfolio', 'dt-the7-core' ),
				'edit_item'             => _x( 'Edit Item',              'backend portfolio', 'dt-the7-core' ),
				'new_item'              => _x( 'New Item',               'backend portfolio', 'dt-the7-core' ),
				'view_item'             => _x( 'View Item',              'backend portfolio', 'dt-the7-core' ),
				'search_items'          => _x( 'Search Items',           'backend portfolio', 'dt-the7-core' ),
				'not_found'             => _x( 'No items found',         'backend portfolio', 'dt-the7-core' ),
				'not_found_in_trash'    => _x( 'No items found in Trash','backend portfolio', 'dt-the7-core' ),
				'parent_item_colon'     => '',
				'menu_name'             => _x( 'Projects', 'backend portfolio', 'dt-the7-core' )
				),
			'public'                => true,
			'publicly_queryable'    => true,
			'show_ui'               => true,
			'show_in_menu'          => true, 
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'project' ),
			'capability_type'       => 'post',
			'has_archive'           => true, 
			'hierarchical'          => false,
			'menu_position'         => 35,
			'supports'              => array( 'author', 'title', 'editor', 'thumbnail', 'comments', 'excerpt', 'revisions' )
		);
}

add_filter( 'presscore_post_type_dt_portfolio_args', 'fg_dt_portfolio_args' );



/**
 * 	PORTFOLIO CATEGORY ARGS
 *
 *  Update the labels for the Posrtfolio Category Taxonomy
 */

function fg_dt_portfolio_category_args() {
	
	return array(
			'labels'                => array(
				'name'              => _x( 'Project Categories', 		'backend portfolio', 'dt-the7-core' ),
				'singular_name'     => _x( 'Project Category', 			'backend portfolio', 'dt-the7-core' ),
				'search_items'      => _x( 'Search in Category', 		'backend portfolio', 'dt-the7-core' ),
				'all_items'         => _x( 'Project Categories', 		'backend portfolio', 'dt-the7-core' ),
				'parent_item'       => _x( 'Parent Project Category', 	'backend portfolio', 'dt-the7-core' ),
				'parent_item_colon' => _x( 'Parent Project Category:', 	'backend portfolio', 'dt-the7-core' ),
				'edit_item'         => _x( 'Edit Category', 			'backend portfolio', 'dt-the7-core' ),
				'update_item'       => _x( 'Update Category', 			'backend portfolio', 'dt-the7-core' ),
				'add_new_item'      => _x( 'Add New Project Category', 	'backend portfolio', 'dt-the7-core' ),
				'new_item_name'     => _x( 'New Category Name', 		'backend portfolio', 'dt-the7-core' ),
				'menu_name'         => _x( 'Project Categories', 		'backend portfolio', 'dt-the7-core' )
				),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'rewrite'               => array( 'slug' => 'project-category' ),
			'show_admin_column'		=> true,
		);
}

add_filter( 'presscore_taxonomy_dt_portfolio_category_args', 'fg_dt_portfolio_category_args' );
