<?php

/**
 *  INCLUDE GUTENBERG
 *
 *  Contains:
 *  01 - inc_allowed_gutenberg_blocks()
 *
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


// https://digwp.com/2018/04/how-to-disable-gutenberg/

// disable for posts
//add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for pages
add_filter('use_block_editor_for_page', '__return_false', 10);

// disable for post types
//add_filter('use_block_editor_for_post_type', '__return_false', 10);


if ( ! function_exists( 'inc_allowed_gutenberg_blocks' ) ) :

/*
https://rudrastyh.com/gutenberg/remove-default-blocks.html#block_slugs

Common

    core/paragraph
    core/image
    core/heading
    core/subhead
    core/gallery
    core/list
    core/quote
    core/audio
    core/cover-image
    core/file
    core/video

Formatting

    core/table
    core/verse
    core/code
    core/freeform
    core/html
    core/preformatted
    core/pullquote

Layout

    core/button
    core/text-columns
    core/more
    core/nextpage
    core/separator
    core/spacer

Widgets

    core/shortcode
    core/archives
    core/categories
    core/latest-comments
    core/latest-posts

Embeds


    core/embed
    core-embed/twitter
    core-embed/youtube
    core-embed/facebook
    core-embed/instagram
    core-embed/wordpress
    core-embed/soundcloud
    core-embed/spotify
    core-embed/flickr
    core-embed/vimeo
    core-embed/animoto
    core-embed/cloudup
    core-embed/collegehumor
    core-embed/dailymotion
    core-embed/funnyordie
    core-embed/hulu
    core-embed/imgur
    core-embed/issuu
    core-embed/kickstarter
    core-embed/meetup-com
    core-embed/mixcloud
    core-embed/photobucket
    core-embed/polldaddy
    core-embed/reddit
    core-embed/reverbnation
    core-embed/screencast
    core-embed/scribd
    core-embed/slideshare
    core-embed/smugmug
    core-embed/speaker
    core-embed/ted
    core-embed/tumblr
    core-embed/videopress
    core-embed/wordpress-tv

*/

	function inc_allowed_gutenberg_blocks( $allowed_block_types ) {
	
	    return array(
			'core/image',
			'core/gallery',
			'core/paragraph',
			'core/heading',
			'core/subhead',
			'core/list',
			'core/table',
			'core/quote',
			'core/pullquote',
			'core/button',
			'core/html',
			'core/separator',
			'core/spacer',
			'core/shortcode',
			'core/embed',
			'core-embed/twitter',
			'core-embed/youtube',
			'core-embed/facebook',
			'core-embed/instagram',
			'core-embed/wordpress'
	    );
	
	}
	
	add_filter( 'allowed_block_types', 'inc_allowed_gutenberg_blocks' );

endif;
