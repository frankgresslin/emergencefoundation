<?php

/**
 *  THEME OPTIONS - AUTHOR BOX & PAGE
 *
 *  Contains:
 *  01 - fg_author_box()
 *  02 - fg_author_bio()
 *  03 - fg_author_meta()
 *  04 - fg_author_page_header()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_author_box' ) ) :

	/**
	 * 	AUTHOR BOX
	 *
	 *  Print author box if author bio is added to user profile
	 *
	 *  @usedby	single-project.php
	 *  @usedby	single.php
	 */
	
	function fg_author_box() { 
	
		if ( is_single() ) {
			
			if ( get_the_author_meta( 'description' ) ) {
			
				$html = '<section id="include-author-box">';
				$html .= 	'<ul id="author-info">';
				$html .= 		'<li id="author-avatar">' . get_avatar( get_the_author_meta( 'ID' ), 64 ) . '</li>';
				$html .= 		'<li id="author-bio">';
				$html .= 			'<h4>';
				$html .=				 __( 'Author: ', 'include' );
				$html .= 				get_the_author();
				$html .= 			'</h4>';
				$html .= 			fg_author_bio();
				$html .= 		'</li>';
				$html .= 	'</ul>';
				$html .= 	fg_author_meta();
				$html .= '</section>';
				
				echo $html; 
			
			}
		}
	}
	
	add_action( 'include_after_loop', 'fg_author_box', 2 );

endif;



if ( ! function_exists( 'fg_author_bio' ) ) :

	/**
	 * 	AUTHOR BIO
	 *
	 *  Shorten author bio and add link to author archive
	 *  Link contains query string that loads only posts of same post type in author archive 
	 *
	 *  @usedby	fg_author_box()
	 */
	
	function fg_author_bio() {                					
		
		$authorUrl = get_author_posts_url( get_the_author_meta( 'ID' ) );
		$querystring = ( 'post' != get_post_type() ) ? '?posttype=' . get_post_type() : '';
		
		// cut description after 30 words and add read more link
		return wp_trim_words( strip_tags( get_the_author_meta( 'description' ) ), 30, '&hellip; <a href="' . $authorUrl . $querystring . '" class="read-more">' . __( 'read more &rarr;', 'include' ) . '</a>' );
		
	}

endif;



if ( ! function_exists( 'fg_author_meta' ) ) :

	/**
	 * 	SOCIAL LINKS
	 *
	 *  Add links to author's social streams. 
	 *  Add querysring to rss feed to get custom post type feed
	 *
	 *  @usedby	fg_author_box()
	 *  
	 *  @link  https://perishablepress.com/best-method-for-email-obfuscation/
	 */
	
	function fg_author_meta() { 
		
		$querystring = ( 'post' != get_post_type() ) ? '?posttype=' . get_post_type() : '';
		
		$twitter = ( NULL !== get_the_author_meta( 'twitter' ) ) ? sanitize_key( get_the_author_meta( "twitter" ) ) : false;
		$facebook = ( NULL !== get_the_author_meta( 'facebook' ) ) ? esc_url( get_the_author_meta( "facebook" ) ) : false;
		$google = ( NULL !== get_the_author_meta( 'googleplus' ) ) ? esc_url( get_the_author_meta( "googleplus" ) ) : false;
		$linkedin = ( NULL !== get_the_author_meta( 'linkedin' ) ) ? esc_url( get_the_author_meta( "linkedin" ) ) : false;
		$user_url = ( NULL !== get_the_author_meta( 'user_url' ) ) ? esc_url( get_the_author_meta( "user_url" ) ) : false;
		$email = ( NULL !== get_the_author_meta( 'email' ) ) ? str_rot13( 'mailto:' . sanitize_email( get_the_author_meta( "email" ) ) ) : false; 
		$rss = ( true === fg_get_option( 'rss' ) ) ? esc_url( get_author_feed_link( get_the_author_meta( 'ID' ) ) . $querystring ) : false;
		
	
		$html = '<ul id="include-author-meta">';
		$html .= ( $twitter ) ? '<li id="author-twitter"><a href="https://twitter.com/' . $twitter . '" target="_blank" rel="noopener noreferrer">' . __( 'Twitter', 'include' ) . '</a></li>' : '';
		$html .= ( $facebook ) ? '<li id="author-facebook"><a href="' . $facebook . '" target="_blank" rel="noopener noreferrer">' . __( 'Facebook', 'include' ) . '</a></li>' : '';
		$html .= ( $google ) ? '<li id="author-google"><a href="' . $google . '" target="_blank" rel="noopener noreferrer">' . __( 'Google+', 'include' ) . '</a></li>' : '';
		$html .= ( $linkedin ) ? '<li id="author-linkedin"><a href="' . $linkedin . '" target="_blank" rel="noopener noreferrer">' . __( 'LinkedIn', 'include' ) . '</a></li>' : '';
		$html .= ( $user_url ) ? '<li id="author-website"><a href="' . $user_url . '" target="_blank" rel="noopener noreferrer">' . __( 'Website', 'include' ) . '</a></li>' : '';
		$html .= ( $email ) ? '<li id="author-email"><a href="' . $email . '">' . __( 'Email', 'include' ) . '</a></li>' : '';
		$html .= ( $rss ) ? '<li id="author-rss"><a href="' . $rss . '" target="_blank" rel="noopener noreferrer">' . __( 'RSS', 'include' ) . '</a></li>' : '';
		$html .= '</ul>';
		
		return $html;
			
	}
	
endif;

	

if ( ! function_exists( 'fg_author_page_header' ) ) :

	/**
	 * 	AUTHOR PAGE HEADER
	 *
	 *  Change header depending on what post type is shown
	 *
	 *  @return  string  $html
	 *
	 *  @usedby	author.php
	 */
	
	function fg_author_page_header() {
		
		if ( is_author() && ( ! empty( get_the_author_meta( 'description' ) ) ) ) {
				
			$left = false;
			$right = false;
			$pto = get_post_type_object( get_post_type() );
			
			// fg_sidebar_left() might not exist in child-theme
			if ( function_exists( 'fg_sidebar_left' ) ) :
			
				$left = ( fg_sidebar_left( false ) ? 'left' : false );
				$right = ( fg_sidebar_right( false ) ? 'right' : false );
				$class = ( $left || $right ) ? 'class="' . $left . $right . '"' : '';
			
			endif;	
			
			$html = '<section id="include-author-page-header">';
			$html .= 	get_avatar( get_the_author_meta( 'ID' ), 128 );
			$html .= 	'<div id="autor-description" ' . $class . '>';		
			$html .= 		get_the_author_meta( 'description' );
			$html .= 	'</div>';
			$html .= 	fg_author_meta();
			$html .= '</section>';		
			$html .= '<h3 id="include-articles-by">' . ucfirst( $pto->labels->name ) . ' by ' . get_the_author() . '</h3>';
			
			echo $html;
			
		}
	}
	
	add_action( 'include_before_loop', 'fg_author_page_header' );

endif;
