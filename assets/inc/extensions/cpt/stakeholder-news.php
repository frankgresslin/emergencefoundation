<?php
	
/*
    Plugin Name: Include Projects
    Plugin URI: https://theme.frankgresslin.net
    Description: Add Custom Post Type Project
    Author: Frank Gresslin
    Version: 1.0.
    Author URI: https://frankgresslin.net
    License: GPLv2
 */
 
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


/**
 *  CUSTOM POST TYPE - PROJECT
 *
 *  Contains: (delete functions not needed in production) 
 *  01 - fg_project_post_type()
 *  02 - fg_project_rewrite_flush()
 *  03 - fg_project_map_meta_cap()
 *  04 - fg_project_taxonomies()
 *  05 - fg_project_updated_messages()
 *  06 - fg_project_custom_help_tab_1()
 *  07 - fg_project_change_title()
 *  08 - fg_project_meta_box_html()
 *  09 - fg_project_save_postdata()
 *  10 - fg_project_add_meta_box()
 *  11 - fg_project_modify_columns()
 *  12 - fg_project_custom_column_content()
 *  13 - fg_project_custom_columns_sortable()
 *  14 - fg_project_columns_orderby()
 *  15 - fg_project_sidebar()
 *  16 - fg_project_get_post_meta()  
 *    
 *  @package include
 *  @since 	 1.0
 *  @link    https://codex.wordpress.org/Post_Types#Custom_Post_Types
 *  @version 1.0.0
 */
 
	
// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; }


if ( ! function_exists( 'fg_stakeholder_news_post_type' ) ) :

	/**
	 * 	Register Custom Post Type 
	 *
	 * 	@return  array  $args    
	 *	@link    hhttps://codex.wordpress.org/Function_Reference/register_post_type
	 */

	function fg_stakeholder_news_post_type() {
	
		$labels = array(
			'name'               		=> _x( 'Stakeholder News', 'post type general name', 'include' ),
			'singular_name'      		=> _x( 'Stakeholder News', 'post type singular name', 'include' ),
			'menu_name'          		=> _x( 'Stakeholder News', 'admin menu', 'include' ),
			'name_admin_bar'     		=> _x( 'Stakeholder News', 'add new on admin bar', 'include' ),
			'add_new'            		=> _x( 'Add New', 'Stakeholder News', 'include' ),
			'add_new_item'       		=> __( 'Add New Stakeholder News', 'include' ),
			'new_item'           		=> __( 'New Stakeholder News Item', 'include' ),
			'edit_item'          		=> __( 'Edit Stakeholder News Item', 'include' ),
			'view_item'          		=> __( 'View Stakeholder News', 'include' ),
			'all_items'          		=> __( 'All Stakeholder News', 'include' ),
			'search_items'       		=> __( 'Search Stakeholder News', 'include' ),
			'parent_item_colon'  		=> __( 'Parent Stakeholder News Item:', 'include' ),
			'not_found'          		=> __( 'No Stakeholder News Item found.', 'include' ),
			'not_found_in_trash' 		=> __( 'No Stakeholder News Item found in Trash.', 'include' ),
		);
	  
/*
		$capabilities = array(
			'publish_posts' 				=> 'publish_projects',
			'edit_posts' 				=> 'edit_projects',
			'edit_others_posts' 		=> 'edit_others_projects',
			'delete_posts' 				=> 'delete_projects',
			'delete_others_posts' 		=> 'delete_others_projects',
			'read_private_posts' 		=> 'read_private_projects',
			'edit_post' 				=> 'edit_project',
			'delete_post' 				=> 'delete_project',
			'read_post' 				=> 'read_project',
		);
*/
	
		$args = array(
			'labels'						=> $labels,
			'public'						=> true,
			'exclude_from_search'		=> true,
			'publicly_queryable'			=> true,
			'show_ui'					=> true,
			'show_in_menu'				=> true,
			'show_in_nav_menus'			=> true,
			'show_in_admin_bar'			=> true,
			'query_var'					=> true,
			'rewrite'					=> array( 'slug' => 'stakeholder-news' ),
			'capability_type'			=> 'post',
			// 'capabilities' 				=> $capabilities,
			'has_archive'				=> true,
			'hierarchical'				=> false,
			'menu_position'				=> 12,
			'menu_icon'					=> 'dashicons-megaphone',
			// 'menu_icon'				=> get_stylesheet_directory_uri() . '/assets/admin/images/generic.png',
			// 'supports'				=> array( 'title','editor','author','thumbnail','excerpt','comments','revisions','custom-fields','post-formats' )
			'supports'					=> array( 'title','editor','thumbnail','excerpt','comments' )
		);
		
		$args = apply_filters( "include_stakeholder_news_args", $args );
		
		register_post_type( 'stakeholder_news', $args );
	  
	}
	
	add_action( 'init', 'fg_stakeholder_news_post_type' );

endif;



if ( ! function_exists( 'fg_stakeholder_news_rewrite_flush' ) ) :

	/**
	 * 	Flushes WordPress rewrite rules 
	 *	@link  https://codex.wordpress.org/Function_Reference/flush_rewrite_rules
	 */

	function fg_stakeholder_news_rewrite_flush() {
	    flush_rewrite_rules();
	}
	
	add_action( 'after_switch_theme', 'fg_stakeholder_news_rewrite_flush' );

endif;



if ( ! function_exists( 'fg_project_map_meta_cap' ) ) :

	/**
	 * 	Map Capabilities 
	 *
	 *	@param   $caps  array  - capabilities of current user
	 *	@param   $cap  string  - apabilities of current user
	 *	@param   $user_id  integer  - id of current user
	 *	@param   $args  array  - 
	 * 	@return  $caps  array  - new capabilities of current user
	 *	@link    http://justintadlock.com/archives/2010/07/10/meta-capabilities-for-custom-post-types
	 */

	function fg_project_map_meta_cap( $caps, $cap, $user_id, $args ) {
	
		if ( 'edit_project' == $cap || 'delete_project' == $cap || 'read_project' == $cap ) {			
			
			$post = get_post( $args[0] );
			$post_type = get_post_type_object( $post->post_type );
	
			$caps = array();
		}
	
		if ( 'edit_project' == $cap ) {
			if ( $user_id == $post->post_author )
				$caps[] = $post_type->cap->edit_posts;
			else
				$caps[] = $post_type->cap->edit_others_posts;
		}
	
		elseif ( 'delete_project' == $cap ) {
			if ( $user_id == $post->post_author )
				$caps[] = $post_type->cap->delete_posts;
			else
				$caps[] = $post_type->cap->delete_others_posts;
		}
	
		elseif ( 'read_project' == $cap ) {
	
			if ( 'private' != $post->post_status )
				$caps[] = 'read';
			elseif ( $user_id == $post->post_author )
				$caps[] = 'read';
			else
				$caps[] = $post_type->cap->read_private_posts;
		}
		
		return $caps;
		
	}
	
	// add_filter( 'map_meta_cap', 'fg_project_map_meta_cap', 10, 4 );

endif;



if ( ! function_exists( 'fg_project_taxonomies' ) ) :

	/**
	 * 	Register Taxonomies 
	 *
	 *	@return   $args_project_categories  array  - project-categories array
	 *	@return   $args_project_tags  array  - project-tags array
	 *	@link     https://codex.wordpress.org/Function_Reference/register_taxonomy
	 */

	function fg_project_taxonomies() {
			
		// Add Sectors
		$labels_project_categories = array(
			'name'              		=> _x( 'Project Categories', 'taxonomy general name', 'include' ),
			'singular_name'     		=> _x( 'Project Category', 'taxonomy singular name', 'include' ),
			'search_items'      		=> __( 'Search Project Categories', 'include' ),
			'all_items'         		=> __( 'All Project Categories', 'include' ),
			'parent_item'       		=> __( 'Parent Project Category', 'include' ),
			'parent_item_colon' 		=> __( 'Parent Project Category:', 'include' ),
			'edit_item'         		=> __( 'Edit Project Category', 'include' ),
			'update_item'       		=> __( 'Update Project Category', 'include' ),
			'add_new_item'      		=> __( 'Add New Project Category', 'include' ),
			'new_item_name'     		=> __( 'New Project Category Name', 'include' ),
			'menu_name'         		=> __( 'Project Categories', 'include' ),
		);
	
		$args_project_categories = array(
			'hierarchical'      		=> true,
			'labels'            		=> $labels_project_categories,
			'show_ui'           		=> true,
			'show_admin_column' 		=> true,
			'query_var'         		=> true,
			'rewrite'           		=> array( 'slug' => 'project-category' ),
			'show_in_nav_menus'			=> true,
		);
		
		$args_project_categories = apply_filters( "include_project_category_args", $args_project_categories );
		
		register_taxonomy( 'project_category', array( 'project' ), $args_project_categories );
		
		
		
		// Add Sectors
		$labels_project_tags = array(
			'name'              		=> _x( 'Project Tags', 'taxonomy general name', 'include' ),
			'singular_name'     		=> _x( 'Project Tag', 'taxonomy singular name', 'include' ),
			'search_items'      		=> __( 'Search Project Tags', 'include' ),
			'all_items'         		=> __( 'All Project Tags', 'include' ),
			'parent_item'       		=> __( 'Parent Project Tag', 'include' ),
			'parent_item_colon' 		=> __( 'Parent Project Tag:', 'include' ),
			'edit_item'         		=> __( 'Edit Project Tag', 'include' ),
			'update_item'       		=> __( 'Update Project Tag', 'include' ),
			'add_new_item'      		=> __( 'Add New Project Tag', 'include' ),
			'new_item_name'     		=> __( 'New Project Tag Name', 'include' ),
			'menu_name'         		=> __( 'Project Tags', 'include' ),
		);
	
		$args_project_tags = array(
			'hierarchical'      		=> false,
			'labels'            		=> $labels_project_tags,
			'show_ui'           		=> true,
			'show_admin_column' 		=> true,
			'query_var'         		=> true,
			'rewrite'           		=> array( 'slug' => 'project-tag' ),
			'show_in_nav_menus'			=> true,
		);
		
		$args_project_tags = apply_filters( "include_project_tag_args", $args_project_tags );
		
		register_taxonomy( 'project_tag', array( 'project' ), $args_project_tags );
		
	}
	
	// add_action( 'init', 'fg_project_taxonomies', 0 );

endif;



if ( ! function_exists( 'fg_project_updated_messages' ) ) :

	/**
	 * 	Update Messages 
	 *
	 *	@param   $messages  array  - default messages array
	 *	@return  $messages  array  - messages array with added projects messages
	 *	@link    https://codex.wordpress.org/Function_Reference/register_post_type
	 */

	function fg_project_updated_messages( $messages = '' ) {
		
		$post             = get_post();
		$post_type        = get_post_type( $post );
		$post_type_object = get_post_type_object( $post_type );
		
		$messages['project'] = array(
			0  => '', // Unused. Messages start at index 1.
			1  => __( 'Project updated.', 'include' ),
			2  => __( 'Custom field updated.', 'include' ),
			3  => __( 'Custom field deleted.', 'include' ),
			4  => __( 'Project updated.', 'include' ),
			/* translators: %s: date and time of the revision */
			5  => isset( $_GET['revision'] ) ? sprintf( __( 'Project restored to revision from %s', 'include' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6  => __( 'Project published.', 'include' ),
			7  => __( 'Project saved.', 'include' ),
			8  => __( 'Project submitted.', 'include' ),
			9  => sprintf(
				__( 'Project scheduled for: <strong>%1$s</strong>.', 'include' ),
				// translators: Publish box date format, see http://php.net/date
				date_i18n( __( 'M j, Y @ G:i', 'include' ), strtotime( $post->post_date ) )
			),
			10 => __( 'Project draft updated.', 'include' ),
		);
	
		if ( $post_type_object->publicly_queryable ) {
			
			$permalink = get_permalink( $post->ID );
	
			$view_link = sprintf( ' <a href="%s" target="_blank" rel="noopener noreferrer">%s</a>', esc_url( $permalink ), __( 'View Project', 'include' ) );
			$messages[ $post_type ][1] .= $view_link;
			$messages[ $post_type ][6] .= $view_link;
			$messages[ $post_type ][9] .= $view_link;
	
			$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
			$preview_link = sprintf( ' <a href="%s" target="_blank" rel="noopener noreferrer">%s</a>', esc_url( $preview_permalink ), __( 'Preview Project', 'include' ) );
			$messages[ $post_type ][8]  .= $preview_link;
			$messages[ $post_type ][10] .= $preview_link;
		}
	
		return $messages;
		
	}
	
	// add_filter( 'post_updated_messages', 'fg_project_updated_messages' );

endif;



if ( ! function_exists( 'fg_project_custom_help_tab_1' ) ) :

	/**
	 * 	Help Tab 
	 *
	 * 	@return  $args  array  array with id, title and content
	 *	@link    https://codex.wordpress.org/Adding_Contextual_Help_to_Administration_Menus
	 */

	function fg_project_custom_help_tab_1() {
	
	  $screen = get_current_screen();
	
	  if ( 'project' != $screen->post_type )
	    return;
	    
	  $content = '<h3>' . __( 'Help Title 1', 'include' ) . '</h3>';
	  $content .= '<p>' . __( 'Help content', 'include' ) . '</p>';
	  // $content .= file_get_contents('http://www.frankgresslin.net/test.html');
	  
	  $args = array(
	    'id'      => 'custom_id_1',
	    'title'   => 'Tab 1',
	    'content' => $content
	  );
	  
	  $screen->add_help_tab( $args );
	  
	}
	
	// add_action( 'admin_head', 'fg_project_custom_help_tab_1' );

endif;



if ( ! function_exists( 'fg_stakeholder_news_change_title' ) ) :

	/**
	 * 	Post Title Placeholder 
	 *
	 *	@param   $title  string  - the default placeholder text
	 *	@return  $title  string  - the updated placeholder text
	 *	@link    https://developer.wordpress.org/reference/hooks/enter_title_here/
	 */

	function fg_stakeholder_news_change_title( $title ) {

	    $screen = get_current_screen();
	    
	    if ( $screen->post_type == 'stakeholder_news' ) {
	    
	        $title = apply_filters( "include_stakeholder_news_title", __( 'Enter Stakeholder News Title Here', 'include' ) ); ;
	    }

	    return $title;
	    
	}
	
	add_filter( 'enter_title_here', 'fg_stakeholder_news_change_title' );

endif;



if ( ! function_exists( 'fg_project_meta_box_html' ) ) :

	/**
	 * 	Meta Box HTML 
	 *
	 *	@param   $post  object  - post object
	 *	@link    https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	 */

	function fg_project_meta_box_html( $post = '' ) {
	
		wp_nonce_field( 'fg_save_project_meta_box', 'fg_project_meta_box_nonce' );
		
		$text_input = get_post_meta( $post->ID, 'text_input', true );
		$text_area = get_post_meta( $post->ID, 'text_area', true );
		$select = get_post_meta( $post->ID, 'select', true );
		$page_list = get_post_meta( $post->ID, 'page_list', true );
		$featured_project = get_post_meta( $post->ID, 'featured_project', true ); 
	?>
	
	<div class="fg_meta_box_container"> 
	
		<label for="text_input">
			<?php _e( 'Text Input: ', 'include' ); ?>
		</label>
		<input type="text" name="text_input" id="text_input" value="<?php echo sanitize_text_field( $text_input ); ?>" class="widefat" />
		<p class="howto" style="margin-top: 0;">
			<?php _e( 'Text Input description', 'include' ); ?>
		</p>
	
		
		
		<label for="text_area">
			<?php _e( 'Text Area: ', 'include' ); ?>
		</label>
		<textarea rows="10" name="text_area" id="text_area" class="widefat"><?php echo wp_kses_post( $text_area ); ?></textarea>
		<p class="howto" style="margin-top: 0;">
			<?php _e( 'Text Area description', 'include' ); ?>
		</p>
		
		
		
		<label for="select">
			<?php _e( 'Select: ', 'include' ); ?>
		</label>	
		<select name="select" id="select" class="widefat">
			<option value="option1" <?php if ( $select == 'option1') { echo 'selected="selected"'; } ?>><?php _e( 'Option 1', 'include' ) ?></option>
			<option value="option2" <?php if ( $select == 'option2') { echo 'selected="selected"'; } ?>><?php _e( 'Option 2', 'include' ) ?></option>
			<option value="option3" <?php if ( $select == 'option3') { echo 'selected="selected"'; } ?>><?php _e( 'Option 3', 'include' ) ?></option>
		</select>
		<p class="howto" style="margin-top: 0;">
			<?php _e( 'Select description.', 'include' ); ?>
		</p>
			
		
		<p>
		    <span class="featured-project-title"><?php _e( 'Featured Project', 'include' )?></span>
		    <div class="featured-projects">
		        <label for="featured_project">
		            <input type="checkbox" name="featured_project" id="featured_project" value="yes" <?php if ( isset ( $featured_project ) ) checked( $featured_project, 'yes' ); ?> />
		            <?php _e( 'Make this a featured project', 'include' )?>
		        </label>
		    </div>
		</p>
	
	
		<label for="page_list">
			<?php _e( 'Page List: ', 'include' ); ?>
		</label>	
		<select name="page_list" id="page_list" class="widefat">
		<?php echo '<option>' . __( 'Select a Page', 'include' ) . '</option>'; ?>
		<?php 
			$oages = get_pages();
			foreach ( $oages as $oage ) {
				$selected = ( $page_list == $oage->post_name ) ? 'selected="selected"' : '';
				echo '<option value="' . $oage->post_name . '"' . $selected . '>' . $oage->post_title . '</option>';
			}
		?>
		</select>
		<p class="howto" style="margin-top: 0;">
			<?php _e( 'Page List description.', 'include' ); ?>
		</p>	
	
	</div>
	 
	<?php 
	}

endif;



if ( ! function_exists( 'fg_project_save_postdata' ) ) :

	/**
	 * 	Save Meta Values 
	 *
	 *	@param   $post_id  integer  - the post id
	 *	@link    https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	 */

	function fg_project_save_postdata( $post_id ) {
	
		if ( ! isset( $_POST['fg_project_meta_box_nonce'] ) || ! wp_verify_nonce( $_POST['fg_project_meta_box_nonce'], 'fg_save_project_meta_box' ) )
			return $post_id;
		
		
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || wp_is_post_revision( $post_id ) )
			return $post_id;
		
		
		if ( 'page' == $_POST['post_type'] ) {
		
			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
			
		} else {
			
			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}
			
			
		if ( isset( $_POST['text_input'] ) ) { 
			update_post_meta( $post_id, 'text_input', sanitize_text_field( $_POST['text_input'] ) ); 
		}
		
		if ( isset( $_POST['text_area'] ) ) { 
			update_post_meta( $post_id, 'text_area', wp_kses_post( $_POST['text_area'] ) ); 
		}
		
		if ( isset( $_POST['select'] ) ) { 
			update_post_meta( $post_id, 'select', sanitize_key( $_POST['select'] ) ); 
		}
		
		if ( isset( $_POST['page_list'] ) ) { 
			update_post_meta( $post_id, 'page_list', sanitize_key( $_POST['page_list'] ) ); 
		}
		
		if ( isset( $_POST['featured_project'] ) ) { 
			update_post_meta( $post_id, 'featured_project', sanitize_key( $_POST['featured_project'] ) ); 
		} else {
	        delete_post_meta( $post_id, 'featured_project');
		}
	}
	
	// add_action( 'save_post', 'fg_project_save_postdata' );

endif;



if ( ! function_exists( 'fg_project_add_meta_box' ) ) :

	/**
	 * 	Add Meta Box 
	 *	@link    https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	 */

	function fg_project_add_meta_box() {
	
	    add_meta_box('project-meta-id', __( 'Project Details', 'include' ), 'fg_project_meta_box_html', 'project', 'side', 'high');
	}
	
// 	add_action('add_meta_boxes', 'fg_project_add_meta_box');

endif;



if ( ! function_exists( 'fg_project_modify_columns' ) ) :

	/**
	 * 	Add Admin Column 
	 */

	function fg_project_modify_columns( $columns ) {
	
	  $new_columns = array( 
	  					'fg_text_input' 	=> __( 'Text Input', 'include' ), 
	  					'fg_post_thumbnail' => __( 'Image', 'include' ) 
	  				);
		  
	  $filtered_columns = array_merge( $columns, $new_columns );
	
	  return $filtered_columns;
	  
	}
	
	// add_filter('manage_project_posts_columns' , 'fg_project_modify_columns');

endif;



if ( ! function_exists( 'fg_project_custom_column_content' ) ) :

	/**
	 * 	Output values 
	 */

	function fg_project_custom_column_content( $column, $post_id ) {
	    
		if ( $column == 'fg_text_input' ) {
		
			$value = get_post_meta( $post_id, 'text_input', true );
		
			echo ( !empty( $value ) ? sanitize_text_field( $value ) : '&nbsp;' );  
		}
		
		if ( $column == 'fg_post_thumbnail' ) {
		
			echo the_post_thumbnail( array( 50, 50 ) );
		
		}
	}
	
	// add_action( 'manage_project_posts_custom_column', 'fg_project_custom_column_content', 10, 2 );

endif;



if ( ! function_exists( 'fg_project_custom_columns_sortable' ) ) :

	/**
	 * 	Make Column Sortable 
	 */

	function fg_project_custom_columns_sortable( $columns ) {
	  
	    $columns['text_input'] = 'text_input';
	
	    return $columns;
	    
	}
	
	// add_filter( 'manage_edit-project_sortable_columns', 'fg_project_custom_columns_sortable' );

endif;



if ( ! function_exists( 'fg_project_columns_orderby' ) ) :

	/**
	 * 	Set Orderby
	 */

	function fg_project_columns_orderby( $query ) {
		
	    if( ! is_admin() ) { return; }
	        
	    $orderby = $query->get( 'orderby');
	 
	    if ( 'text_input' == $orderby ) {
	        $query->set('meta_key','text_input');
	        $query->set('orderby','meta_value');
	    }
	}
	
	// add_action( 'pre_get_posts', 'fg_project_columns_orderby' );

endif;



if ( ! function_exists( 'fg_project_sidebar' ) ) :

	/**
	 * 	Register Sidebar 
	 *	@link  https://codex.wordpress.org/Function_Reference/register_sidebar
	 */

	function fg_project_sidebar() {
	    
	    $sidebar_args = array(
	    	'name'          	=> __( 'Projects Sidebar', 'include' ),
	    	'id'            	=> "projects-sidebar",
	    	'description'   	=> '',
	    	'class'         	=> '',
	    	'before_widget' 	=> '<section id="%1$s" class="widget %2$s">',
	    	'after_widget'  	=> "</section>",
	    	'before_title'  	=> '<h4 class="widget-title">',
	    	'after_title'   	=> "</h4>" 
	    );
	    
	    $sidebar_args = apply_filters( "include_sidebar_args", $sidebar_args );
	    
	    register_sidebar( $sidebar_args );
	    
	}
	
	// add_action( 'widgets_init', 'fg_project_sidebar' );

endif;
