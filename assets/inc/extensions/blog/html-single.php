<?php

/**
 *  HTML POSTS
 *    
 *  Contains:
 *  01 - fg_post_navigation()
 *  02 - fg_social_sharing()
 *  03 - fg_related_posts()
 *    
 *  @package include
 *  @since 	 1.0
 *  @version 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) { exit; } 


if ( ! function_exists( 'fg_post_navigation' ) ) :

	/**
	 * 	POST NAVIGATION
	 *
	 *  Sets 'back' link & title tag dependong on post type 
	 *
	 *  @usedby	 single.php
	 *  @usedby	 single-project.php
	 */
	
	function fg_post_navigation() {
		
		if ( is_single() ) {
			
			$posttype = get_post_type();
						
			$pto = get_post_type_object( $posttype );
			$back_label = ( is_singular( $posttype ) && ( $posttype != 'post' ) ) ? ucwords( $pto->labels->name ) : 'Blog';	
			$back_link = ( 'post' != $posttype ) ? home_url( '/' . $pto->rewrite['slug'] ) : get_permalink( get_option( 'page_for_posts' ) );
			
			$html = '<nav id="include-post-navigation">'; 
		
				$html .= '<div id="prev">' . get_previous_post_link( '%link', '%title' ) . '</div>';
				$html .= '<div id="back"> <a href="' . esc_url( $back_link ) . '" title="Back to ' . $back_label . '"><i class="fa fa-th"></i></a> </div>';
				$html .= '<div id="next">' . get_next_post_link( '%link', '%title' ) . '</div>';
		
			$html .= '</nav>';
				
			echo $html;
			
		}
	}
	
	add_action( 'include_after_loop', 'fg_post_navigation', 3 );

endif;



if ( ! function_exists( 'fg_social_sharing' ) ) :
	
	/**
	 * 	SOCIAL SHARING
	 *
	 *  Sets 'back' link & title tag dependong on post type 
	 *
	 *  @usedby	 single.php
	 *  @usedby	 single-project.php
	 */
	
	function fg_social_sharing() {
		
		if( is_single() ) {
			
			if ( function_exists( 'the_ratings' ) ) { the_ratings(); }
					
			if ( class_exists( 'ESSB_Manager' ) ) {
			
				echo do_shortcode( '[easy-social-share buttons="facebook,twitter,linkedin,mail,more,google,pinterest,subscribe" morebutton="1" morebutton_icon="plus" counters=0 style="button"]' );
			
			} elseif ( class_exists( 'AddThisMaximumPlugin' ) ) {
				
				echo do_shortcode( '  [addthis tool="addthis_inline_share_toolbox_8se6"]  ' );
				
			}		
		}
	}
	
	// add_action( 'include_after_loop', 'fg_social_sharing', 1 );

endif;



if ( ! function_exists( 'fg_related_posts' ) ) :
	
	/**
	 * 	RELATED POSTS
	 *
	 *  Echo related posts frpm 'Manual Related Posts' plugin
	 *
	 *  @usedby	 single.php
	 */
	
	function fg_related_posts() {
		
		if( is_single() ) {
			
			echo ( function_exists( 'include_related_posts_shortcode' ) ) ? do_shortcode( '[include_related_posts]' ) : '';
				
		}
	}
	
	//add_action( 'include_after_loop', 'fg_related_posts', 4 );

endif;
