module.exports = function (grunt) {
    // Configure grunt
    grunt.initConfig({
        watch: {
        	sass: {
        	    files: [ 'assets/sass/**/*.scss' ],
        	    tasks: [ 'sass', 'cssmin' ],
        	    options: {
        	          livereload: true
        	    }
        	},
            js: {
                files: [ 'assets/js/*.js', 'assets/js/override/*.js', '!assets/js/*.min.js' ],
                tasks: [ 'uglify' ]
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    lineNumbers: true
                },
                files: {
                    'assets/css/include-child.css' : ['assets/sass/include-child.scss']
                }
            }
        },
        cssmin: {
        	css: { 
	            files: {
	                'assets/css/include-child.min.css' : ['assets/css/include-child.css']
	            }
	    	}
        },
        csslint: {
          strict: {
          	options: {
				'duplicate-background-images': false,
				'compatible-vendor-prefixes': false,
				'display-property-grouping': true,
				'overqualified-elements': false,
				'unqualified-attributes': true,
				'duplicate-properties': false,
				'universal-selector': true,
				'star-property-hack': false,
				'qualified-headings': false,
				'adjoining-classes': false,
				'known-properties': false,
				'fallback-colors': false,
				'unique-headings': false,
				'regex-selectors': true,
				'vendor-prefix': true,
				'outline-none': false,
				'text-indent': true,
				'empty-rules': false,
				'box-sizing': false,
				'font-sizes': false,
				'zero-units': false,
				'box-model': false,
				'gradients': true,
				'important': false,
				'floats': false,
				'errors': false,
				'ids': false
          	    },
            src: ['assets/css/*.css']
          }
        },
        uglify: {
            options: {
                mangle: {
                    except: ['jQuery']
                },
                sourceMap: false
            },
            custom: {
                files: {
                    'assets/js/main.min.js': [
                        'assets/js/*.js',
                        '!assets/js/*.min.js'
                    ],
                    'assets/js/override/the7-main.min.js': [
                        'assets/js/override/*.js',
                        '!assets/js/override/*.min.js'
                    ]
                }
            }
        }
    });

	grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-csslint');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('scripts', ['uglify']);
    grunt.registerTask('styles', ['sass', 'cssmin', 'csslint']);
    grunt.registerTask('dev', ['watch']);
}
