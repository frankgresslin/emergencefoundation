<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the <div class="wf-container"> and all content after
 *
 * @package The7
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( presscore_is_content_visible() ): ?>

			</div><!-- .wf-container -->			
		</div><!-- .wf-wrap -->		
	
	</div><!-- #main -->
	
			<?php  
					
				if ( 559 == inc_get_ancestor_id() ) {
					
					// GET IN TOUCH
						
					$vc_custom_css = false;
				
					// get vc custom css from post meta
					$post_meta = get_post_meta( '508' );
					
					if ( isset( $post_meta[ '_wpb_shortcodes_custom_css' ][0] ) ) {
						
						$array_items = $post_meta[ '_wpb_shortcodes_custom_css' ];
						
						$vc_custom_css = '<style>';
						
						foreach ( $array_items as $array_item ) {
							$vc_custom_css .= $array_item;
						}
						
						$vc_custom_css .= '</style>';
					}
					
					echo $vc_custom_css;
					
					
					// get templetera template content
					$query = new WP_Query( array( 'post_type' => 'templatera', 'p' => '508' ) );
				
					if ( $query->have_posts() ) {
						
						while ( $query->have_posts() ) { 
							
							$query->the_post();
							
							WPBMap::addAllMappedShortcodes();
						
							the_content();
						}
					}
					
					wp_reset_postdata();
					
				}
				
			?>
			
		


	<?php
	if ( presscore_config()->get( 'template.footer.background.slideout_mode' ) ) {
		echo '</div>';
	}

	do_action( 'presscore_after_main_container' );
	?>

<?php endif // presscore_is_content_visible ?>

	<a href="#" class="scroll-top"><span class="screen-reader-text"><?php esc_html_e( 'Go to Top', 'the7mk2' ) ?></span></a>
</div><!-- #page -->

<?php wp_footer() ?>

<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
 
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
 
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>