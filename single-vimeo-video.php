<?php
/**
 * The Template for displaying all single posts.
 *
 * @package The7
 * @since   1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header( 'single' );
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'header-main' ) ?>

	<?php if ( presscore_is_content_visible() ): ?>

		<?php do_action( 'presscore_before_loop' ) ?>

		<div id="content" class="content" role="main">

			<?php if ( post_password_required() ): ?>

				<article id="post-<?php the_ID() ?>" <?php post_class() ?>>

					<?php
					do_action( 'presscore_before_post_content' );
					the_content();
					do_action( 'presscore_after_post_content' );
					?>
					
					<?php  
						
						// POST NAVIGATION
						
						// the7 post naviagtion
						// echo presscore_new_post_navigation();
						
						// include post navigation
						// fg_post_navigation();
								
						// RELATED POSTS
						
						presscore_display_related_posts();	
												
					?>	

				</article>

			<?php else: ?>

				<?php 
				
				// the7 tempate part call
				// get_template_part( 'content-single', str_replace( 'dt_', '', get_post_type() ) );
				
				// incude template part - calling the modified the7 tempate part
				get_template_part( 'template-parts/content', 'single-vimeo-video' ); 
				
				?>

			<?php endif ?>
			
			<?php // do_action( 'include_after_loop'); ?>

			<?php comments_template( '', true ) ?>

		</div><!-- #content -->

		<?php do_action( 'presscore_after_content' ) ?>

	<?php endif // content is visible ?>

<?php endwhile; endif; // end of the loop. ?>

<?php get_footer() ?>